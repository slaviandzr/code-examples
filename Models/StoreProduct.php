<?php

namespace common\models;

use Yii;
use yii\data\Pagination;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "{{%store.product}}".
 *
 * @property integer $id
 * @property integer $retailer_id
 * @property string $offer
 * @property integer $datafeed_id
 * @property boolean $active
 * @property boolean $available
 * @property string $name
 * @property string $type_prefix
 * @property string $vendor
 * @property string $model
 * @property string $description
 * @property string $currency
 * @property string $price
 * @property string $old_price
 * @property string $url
 * @property string $code
 * @property integer $category
 * @property integer $gender *
 * @property string $categories_raw
 * @property string $market_category
 * @property string $country_of_origin
 * @property string $barcode
 * @property string $params
 * @property integer $substitution_product_id
 * @property integer $likes_count
 *
 * @property StorePicture[] $storePictures
 * @property StoreDatafeed $datafeed
 * @property StoreFilterItem $category0
 * @property StoreFilterItem $gender0
 * @property StoreRetailer $retailer
 * @property StoreProductFilterItem[] $storeProductFilterItems
 */
class StoreProduct extends \yii\db\ActiveRecord
{
    const ID_NO_SUBSTITUTION_RESULT = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%store.product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['params'], 'safe'],
            [['retailer_id', 'datafeed_id', 'category', 'gender', 'substitution_product_id', 'likes_count'], 'integer'],
            [['offer'], 'required'],
            [['offer', 'name', 'type_prefix', 'vendor', 'model', 'description', 'currency', 'url', 'code', 'categories_raw', 'market_category', 'country_of_origin', 'barcode'], 'string'],
            [['active', 'available'], 'boolean'],
            [['price', 'old_price'], 'number'],
            [['datafeed_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreDatafeed::className(), 'targetAttribute' => ['datafeed_id' => 'id']],
            [['category'], 'exist', 'skipOnError' => true, 'targetClass' => StoreFilterItem::className(), 'targetAttribute' => ['category' => 'id']],
            [['gender'], 'exist', 'skipOnError' => true, 'targetClass' => StoreFilterItem::className(), 'targetAttribute' => ['gender' => 'id']],
            [['retailer_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreRetailer::className(), 'targetAttribute' => ['retailer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'retailer_id' => Yii::t('app', 'Retailer ID'),
            'offer' => Yii::t('app', 'Offer'),
            'datafeed_id' => Yii::t('app', 'Datafeed ID'),
            'active' => Yii::t('app', 'Active'),
            'available' => Yii::t('app', 'Available'),
            'name' => Yii::t('app', 'Name'),
            'type_prefix' => Yii::t('app', 'Type Prefix'),
            'vendor' => Yii::t('app', 'Vendor'),
            'model' => Yii::t('app', 'Model'),
            'description' => Yii::t('app', 'Description'),
            'currency' => Yii::t('app', 'Currency'),
            'price' => Yii::t('app', 'Price'),
            'old_price' => Yii::t('app', 'Old Price'),
            'url' => Yii::t('app', 'Url'),
            'code' => Yii::t('app', 'Code'),
            'category' => Yii::t('app', 'Category'),
            'gender' => Yii::t('app', 'Gender'),
            'categories_raw' => Yii::t('app', 'Categories Raw'),
            'market_category' => Yii::t('app', 'Market Category'),
            'country_of_origin' => Yii::t('app', 'Country Of Origin'),
            'barcode' => Yii::t('app', 'Barcode'),
            'params' => Yii::t('app', 'Params'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorePictures()
    {
        return $this->hasMany(StorePicture::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainPicture()
    {
        return $this->hasOne(StorePicture::className(), ['product_id' => 'id'])
            ->where(['main' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatafeed()
    {
        return $this->hasOne(StoreDatafeed::className(), ['id' => 'datafeed_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(StoreFilterItem::className(), ['id' => 'category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender0()
    {
        return $this->hasOne(StoreFilterItem::className(), ['id' => 'gender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRetailer()
    {
        return $this->hasOne(StoreRetailer::className(), ['id' => 'retailer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreProductFilterItems()
    {
        return $this->hasMany(StoreProductFilterItem::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(Objects::className(), ['object_id' => 'object_id'])
            ->viaTable(SmMediaProducts::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(SmMediaProducts::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOldProductResults()
    {
        return $this->hasMany(OldProductsResults::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubstitutionProduct()
    {
        return $this->hasOne(self::className(), ['id' => 'substitution_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLike()
    {
        return $this->hasOne(ProductsLikes::className(), ['product_id' => 'id'])->andWhere(['user_id' => Yii::$app->user->getId()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLikes()
    {
        return $this->getLikes()->andWhere(['user_id' => Yii::$app->user->getId()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(ProductsLikes::className(), ['product_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return StoreProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StoreProductQuery(get_called_class());
    }

    public static function getNextReadyOldId($currId){
        $nextId = StoreProduct::find()->select('p.id')->alias('p')
            ->innerJoinWith(['results r' => function($query)  {
                $query->select('r.rank, r.product_id');
                $query->andWhere([
                    'r.rank' => [SmMediaProducts::RANK_SIMILAR_PRODUCT, SmMediaProducts::RANK_SAME_PRODUCT],
                ]);
            }])
            ->leftJoin(StoreRetailer::tableName() . ' as sr', 'sr.id = p.retailer_id')
            ->andWhere([
                '[[p.active]]' => false, '[[p.substitution_product_id]]' => null, 'sr.active' => true
            ])->andWhere(['not in', 'p.id', $currId])
            ->orderBy('RANDOM()')->limit(1)->asArray()->one();

        return (isset($nextId['id'])) ? $nextId['id'] : null;
    }

    public static function getStaticLink($product_id, $isSimple = false){
        $product = self::findOne((int)$product_id);
        if(empty($product)) return null;
        return $product->getLink($isSimple);

    }

    /*
     * returns link to product from DB
     * can be simple (without admitad redirects)
     */
    public function getLink($isSimple = false){
        if($isSimple){
            if ( strripos($this->url, 'admitad.com') ){
                if( empty(parse_url($this->url)['query']) ){
                    return $this->url;
                } else {
                    parse_str(parse_url($this->url)['query'], $output);
                    if(!empty($output['ulp'])) {
                        $url = $output['ulp'];
                        return $url;
                    } else return $this->url;
                }

            }

        }

        return $this->url;
    }

    public function isFindedAlternaive(){
        return $this->substitution_product_id;
    }

    /*
     *
     *
     */
    public function toggleLike(){
        if ( Yii::$app->user->isGuest ){
            $session = Yii::$app->session;
            if($this->isLiked()){
//            $session['product_likes'][$this->id] = false;
                $productLikes = $session['product_likes'];
                if(!empty($productLikes[$this->id])) unset($productLikes[$this->id]);
                $session['product_likes'] = $productLikes;
                if($this->likes_count > 0){
                    $this->updateCounters(['likes_count' => -1]);
                }
            } else {
                $productLikes = $session['product_likes'];
                $productLikes[$this->id] = true;
                $session['product_likes'] = $productLikes;
                $this->updateCounters(['likes_count' => 1]);
            }
        } else {
            if($this->isLiked()){
                foreach ($this->getUserLikes()->all() as $like){
                    $like->delete();
                }
                if($this->likes_count > 0){
                    $this->updateCounters(['likes_count' => -1]);
                }
            } else {
                $like = new ProductsLikes();
                $like->date = time();
                $like->user_id = Yii::$app->user->getId();
                $like->product_id = $this->id;
                $like->save();
                $this->updateCounters(['likes_count' => 1]);
            }
        }
    }

    /*
     *
     */
    public function isLiked($byUserId = null, $withCookie = null){
        return ProductsLikes::isLiked($this->id);
    }

    public function getFirstPicture(){
        return StorePicture::find()->where(['product_id' => $this->id])->orderBy('main')->limit(1)->one();
    }

    /**
     * @param $params array Search params
     * @return array products
     */
    public static function getOldProductsData($params = []){
        // Set default values to input params
        $default_params = array (
            'offset' => 0,
            'mediaStatuses' => [UsersSmMedia::STATUS_ACTIVE],
            'accountTypes' => [],
            'pageSize' => 20,
            'minPriority' => SmMediaProducts::PRIORITY_60,
            'maxPriority' => null, // use only with minPriority
            'existsSubstitutionResults' => true,
            'sites' => [],
            'retailers' => [],
            'getProductInfo' => false,
            'getRetailerData' => false,
            // так же получать товары с выключенным ретейлером
            'orRetailerNotActive' => false
        );

        $params = array_merge($default_params, $params);

        $selectSub1 = [
            '[[p.id]] AS pid', '[[p.name]] AS name', '[[p.category]] AS category_id', '[[p.gender]] AS gender', '[[r.name]] AS retailer'
        ];

        $conditionArray = ['and'];

        if( isset($params['orRetailerNotActive']) ){
            if ($params['orRetailerNotActive']){
                array_push($conditionArray, [
                    'or',
                    ['not', '[[p.active]]'],
                    ['not', '[[r.active]]']
                ]);
            } else {
                array_push($conditionArray, ['not', '[[p.active]]']);
            }
        }

        array_push($conditionArray, ['[[p.substitution_product_id]]' => null]);
        if(!empty($params['mediaStatuses'])){
            array_push($conditionArray, ['[[m.status]]' => $params['mediaStatuses']]);
        }

        if(!empty($params['minPriority'])){
            array_push($conditionArray, ['>=', '[[mp.priority]]', (int)$params['minPriority']]);
        }

        if($params['maxPriority']) {
            array_push($conditionArray, ['<=', '[[mp.priority]]', (int)$params['maxPriority']]);
        }

        $sub1 = ( new \yii\db\Query() )->select($selectSub1)->from(['p' => StoreProduct::tableName()]);
        $sub1->leftJoin(StoreRetailer::tableName() . ' r', 'p.retailer_id = r.id');
        $sub1->innerJoin(SmMediaProducts::tableName(). ' mp', 'mp.product_id = p.id');
        $sub1->innerJoin(Objects::tableName(). ' o', 'o.object_id = mp.object_id');
        $sub1->innerJoin(UsersSmMedia::tableName(). ' m', 'm.media_id = o.media_id');

        if ( !empty($params['sites']) ){
            $account_ids = [];

            foreach ($params['sites'] as $site){
                array_push($account_ids, $site);
            }

            if(!empty($account_ids)){
                array_push($conditionArray, ['[[m.account_id]]' => $account_ids]);
            }
        }

        if ( !empty($params['retailers']) ){
            $retailer_ids = [];

            foreach ($params['retailers'] as $retailer){
                array_push($retailer_ids, $retailer);
            }

            if(!empty($retailer_ids)){
                array_push($conditionArray, ['[[p.retailer_id]]' => $retailer_ids]);
            }
        }

        if(!empty($params['accountTypes']) ){
            $sub1->innerJoin(UsersSmAccounts::tableName() . ' a', 'a.account_id = m.account_id');

            if(!empty($params['accountTypes'])){
                array_push($conditionArray, ['[[a.type]]' => $params['accountTypes']]);
            }
        }

        $sub1->where($conditionArray);

        $mainPictureQuery = ( new \yii\db\Query() )->select('id')->from(['pic' => StorePicture::tableName()])
            ->where('[[pic.product_id]] = sub1.pid')
            ->orderBy('main DESC')->limit(1);

        $selectSub2 = [
            '*', 'count(*)', 'picture_id' => $mainPictureQuery
        ];

        if( isset($params['existsSubstitutionResults']) ){
            $start = 'EXISTS';

            $minPeriodOfOldResultsExpression = '';
            if( !empty($params['minPeriodOfOldResults']) ){
                $minPeriodOfOldResultsExpression = new Expression("to_timestamp(opr.date) > now() - interval '".$params['minPeriodOfOldResults']."'");
            }

            $existsSubstitutionResultsQuery = new Expression(' ( ' . $start . ' ( '.
                ( new \yii\db\Query() )->select('id')->from(['opr' => OldProductsResults::tableName()])
                    ->andWhere('[[opr.product_id]] = sub1.pid')
                    ->andWhere($minPeriodOfOldResultsExpression)
                    ->createCommand()->getRawSql()
                .' ) ) ');

            $selectSub2['existsSubstitutionResults'] = $existsSubstitutionResultsQuery;
        }

        $sub2 = ( new \yii\db\Query() )->select($selectSub2)->from(['sub1' => $sub1])
            ->groupBy(['pid', 'name', 'retailer', 'category_id', 'gender'])
            ->orderBy('count(*) DESC');

        $mainQuery = ( new \yii\db\Query() )->select('*')->from(['sub2' => $sub2]);

        if( isset($params['getProductInfo']) && $params['getProductInfo'] ){
            $mainQuery->addSelect('p2.name AS pname');
            $mainQuery->leftJoin(StoreProduct::tableName() . ' p2', 'p2.id = pid');
        }
        if( isset($params['getRetailerData']) && $params['getRetailerData'] ){
            $mainQuery->leftJoin(StoreRetailer::tableName() . ' r2', 'r2.id = retailer_id');
        }

        if( isset($params['existsSubstitutionResults']) ){

            if ( $params['existsSubstitutionResults'] ){
                $mainQuery->andWhere(['[[sub2.existsSubstitutionResults]]' => true]);
            } else {
                $mainQuery->andWhere(['[[sub2.existsSubstitutionResults]]' => false]);
            }
        }

        $pages = new Pagination([
            'totalCount' => (clone $mainQuery)->count(),
            'pageSize' => $params['pageSize'],
        ]);
        $pages->pageSizeParam = false;

        $mainQuery->offset($pages->offset);
        $mainQuery->limit($pages->limit);

        $results = $mainQuery->all();

        return array($results, $params, $pages);
    }


    /**
     * @param $params array Search params
     * @return array objects data
     */
    public static function getObjectsByParams($params = []){
        // Set default search params
        $default_params = array (
            'mediaStatuses' => [],
            'productId' => null,
            'minPriority' => SmMediaProducts::PRIORITY_60
        );

        $params = array_merge($default_params, $params);

        $select = [
            'o.object_id', 'o.media_id', 'smp.product_id', 'm.media_id_api', 'm.status', 'smp.priority', 'smp.rank', 'smp.distance', 'a.moderation_type', 'a.type'
        ];

        $conditionArray = ['and'];

        if(!empty($params['mediaStatuses'])){
            array_push($conditionArray, ['[[m.status]]' => $params['mediaStatuses']]);
        }

        if(!empty($params['minPriority'])){
            array_push($conditionArray, ['>=', '[[smp.priority]]', (int)$params['minPriority']]);
        }

        if(!empty($params['productId'])){
            array_push($conditionArray, ['[[smp.product_id]]' => (int)$params['productId']]);
        }

        $query = ( new \yii\db\Query() )->select($select)->from(['o' => Objects::tableName()]);

        $query->innerJoin(UsersSmMedia::tableName() . ' m', 'm.media_id = o.media_id');
        $query->innerJoin(UsersSmAccounts::tableName() . ' a', 'a.account_id = m.account_id');
        $query->innerJoin(SmMediaProducts::tableName(). ' smp', 'smp.object_id = o.object_id');

        $query->orderBy('a.moderation_type DESC, m.status');

        $query->where($conditionArray);

        return $query->all();
    }

    /**
     * @param $params array Search params
     * @return array old products results data
     */
    public static function getOldProductResultsByParams($params = []){
        $default_params = array (
            'active' => true,
            'productId' => null,
            'getRetailer' => false
        );

        $params = array_merge($default_params, $params);

        $select = [
            'opr.id', 'opr.product_id', 'opr.substitution_product_id', 'opr.picture_id', 'opr.distance', 'opr.rank'
        ];

        $query = ( new \yii\db\Query() )->select($select)->from(['opr' => OldProductsResults::tableName()]);

        $query->innerJoin(StoreProduct::tableName() . ' sp', 'sp.id = opr.substitution_product_id');

        $conditionArray = ['and'];

        array_push($conditionArray, ['[[opr.product_id]]' => (int)$params['productId']]);

        array_push($conditionArray, ['!=','[[opr.substitution_product_id]]',(int)$params['productId']]);

        if(!empty($params['productId'])){
            array_push($conditionArray, ['[[opr.product_id]]' => (int)$params['productId']]);
        }

        if(!empty($params['active'])){
            array_push($conditionArray, ['[[sp.active]]' => (int)$params['active']]);
        }

        if( isset($params['getRetailer']) && $params['getRetailer'] ){
            $query->innerJoin(StoreRetailer::tableName() . ' sr', 'sr.id = sp.retailer_id');
            $query->addSelect('sr.id AS rid, sr.name AS rname, sr.ad_model AS rad_model, sr.active AS ractive');
        }

        $query->where($conditionArray);

        $query->orderBy('opr.distance');

        return $query->all();
    }

    public static function getOldProductsIDsForSendingToApi(){
        $query = (new Query())->select('sp.id')->from(StoreProduct::tableName() . ' sp')
            ->innerJoin(OldProductsResults::tableName() . ' opr', 'sp.id = opr.product_id')
            ->where(['sp.active' => false, 'sp.substitution_product_id' => null])
            ->groupBy('sp.id')->limit(1000);

        return $query->all();
    }

}
