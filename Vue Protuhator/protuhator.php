<?php
/**
 * Created by PhpStorm.
 * User: lukoyanov
 * Date: 15.08.18
 * Time: 13:07
 */

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$azureStrategy = new \common\components\FilerAzureStrategy();
$adModels = \common\models\StoreRetailer::getAdModels();
?>

<div class="container lost-product-form" id="app-container">

    <!--    <div class="js-alerts">-->
    <div class="">
        <div v-for="notify in notifications" class="alert " :class="['alert-' + notify.type]" role="alert"
             :key="notify.key">
            <span v-html="notify.text"></span>
        </div>
    </div>

    <?php $form = ActiveForm::begin([
        'action' => Url::to(['operations/add-alternative-product']),
    ]); ?>

    <div class="row">

        <div class="col-sm-4">
            <div>
                <img v-bind:src="productImg" alt="" class="responsive" style="max-width: 100%;">
            </div>

            <div class="row">
                <div class="col-sm-12">
                    Статус:
                    <template v-if="product.active">
                        не протух
                    </template>
                    <template v-else>
                        протух,
                        <span v-if="product.substitution_product_id > 0">
    заменен на {{product.substitution_product_id}}, показываться в списке протухатора не будет
</span>
                        <span v-else-if="product.substitution_product_id === 0">
    помечен замененным, но без результатов, показываться в списке протухатора не будет
</span>
                        <span v-else>
пока не заменен, показывается в списке протухатора
</span>
                    </template>
                </div>
                <div class="col-sm-12">
                    Магазин:
                    <template v-if="product.retailer">
                        {{product.retailer.name}}
                        <span v-if="product.retailer.active">
    (активен)
    </span>
                        <span v-else>
(выключен)
</span>
                    </template>
                    <template v-else>
                        -
                    </template>
                </div>
                <div class="col-sm-12">
                    Список стран со всех паблишеров, чьи объекты представлены:
                    <span class="label label-default" v-for="country of objectsCountriesList">
    {{ country }}
</span>
                </div>

                <div id="objects">
                    <div class="col-sm-12">
                        <label for="">
                            Выделите объекты или отмодерируйте их вручную:
                        </label>
                    </div>

                    <div class="checkbox" v-for="object in objects">
                        <label class="row bg-green"
                               v-bind:class="[ (object.moderation_type === 'M' && object.type === 'S') ? 'border-rank-4' : '',
( object.is_substituted ) ? 'substituted' : 'not-substituted']">
                            <div class="col-sm-4">
                                <img v-bind:src="object.img" alt="" class="img-responsive cloudzoom"
                                     v-bind:data-cloudzoom="`zoomImage: '${object.img}', zoomPosition: 0`">
                            </div>

                            <div class="col-sm-8">
                                <div>
                                    <!--                                    <input type="checkbox" v-model="object.checked" v-bind:id="object.object_id">-->
                                    <input type="radio" name="pickedObject" v-bind:value="object"
                                           v-model="pickedObject">
                                </div>

                                <div>
                                    <a v-bind:href="object.moderate_link" target="_blank">Объект #{{object.object_id}} в
                                        посте
                                        #{{object.media_id}}</a>
                                </div>

                                <div>
                                    {{ getStatusStr(object.status) }}
                                </div>

                                <div>
                                    Приоритет: {{ object.priority }}
                                </div>

                                <div>
                                    Тип модерации: {{ object.moderation_type }}
                                </div>

                                <div>
                                    Ранк: {{ object.rank }}
                                </div>

                                <div>
                                    Distance: {{ object.distance }}
                                </div>

                                <div>
                                    Аккаунт: <a v-bind:href="object.acc_moderate_link" target="_blank">{{object.username_api}}</a>
                                    ({{ getTypeStr(object.type) }})
                                </div>

                                <div>
                                    Страны акка:
                                    <template v-if="isEmpty(object.account_countries)">
                                        <span class="label label-default">Все</span>
                                    </template>
                                    <template v-else>
<span class="label label-default" v-for="c in object.account_countries">
    {{ c[1] }}
</span>
                                    </template>
                                </div>

                                <!--                                <div>-->
                                <!--                                    Уже заменен (раз): <span class="label label-default">{{ object.protuhator_substitutions.length }}</span>-->
                                <!--                                </div>-->

                            </div>
                        </label>

                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-8">
            <div class="form-group">
                <div>
                    <label for="is_automated">
                        <input type="checkbox" name="is_automated" id="is_automated" v-model="isAutomated">
                        Товар может быть обработан автоматически
                    </label>
                </div>
                <div>
                    <a v-on:click.prevent="setResult($event)" class="btn btn-primary" :disabled="isSetResultDisabled">
                        Применить выбранный результат <span v-if="pickedResult">#{{pickedResult}}</span> для {{
                        countChecked }} объектов
                    </a>
                    <span v-if="product.substitution_product_id === 0">
    <a v-on:click.prevent="lostNoResult(product.id, 'no', $event)"
       class="btn btn-primary">Пометить товар не замененным</a>
</span>
                    <span v-else>
<a v-on:click.prevent="lostNoResult(product.id, 0, $event)" class="btn btn-primary">Пометить товар замененным
без результата</a>
</span>
                </div>
                <div class="m-t-10" v-if="nextId">
                    <a v-on:click.prevent="toNext(product.id, nextId, $event)" class="btn btn-success">Завершить и к
                        следующему</a>
                </div>
            </div>
            <div>

            </div>
            <div class="container-fluid my-overflow-y">
                <div class="row country-filter">

                    <label for="">
                        Протухший товар остатся в результатах поиска, но не выводится
                        в виджете. Выделите объекты:
                    </label>

                    <template v-for="(result, key, index) in oldResults" v-if="filterResults(result)">
                        <template v-if="is3Del(index)">
                            <div class="clearfix"></div>
                        </template>
                        <div class="col-sm-4 filter-item">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="alternative" v-bind:value="result.id"
                                           v-model="pickedResult">
                                    <img v-bind:src="result.img" alt="" class="img-responsive">
                                </label>
                                <div>ID замены: {{ result.id }}</div>
                                <div>ID товара: {{result.substitution_product_id}}</div>
                                <div>URL товара: <a :href="result.url" target="_blank">link</a></div>
                                <div>Price: {{ result.price | formatPrice(result.currency) }}</div>
                                <strong>{{result.name + ' (' + result.vendor + ')'}}</strong>
                                <div>
                                    Category: <span>{{ result.category_name }}</span>
                                </div>
                                <div>
                                    ID картинки: {{result.picture_id}}
                                    <a href="#" v-on:click.prevent="changeImage(result)">
                                        Заменить
                                    </a>
                                </div>
                                <div>Магазин: {{result.rname}} ({{getAdModelStr(result.rad_model)}})</div>

                                <div>
                                    <a data-toggle="collapse" :href="['#collapse' + result.id]">Показать страны</a>

                                    <div :id="['collapse' + result.id]" class="collapse">
                                        <template v-if="isEmpty(result.countries)">
                                            Все страны
                                        </template>
                                        <template v-else>
<span class="label " v-for="c in result.countries"
      v-bind:class="[ (filterCountry == c[0]) ? 'label-success' : 'label-default' ]"
      style="white-space: normal;">
    {{c[1]}}
</span>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </template>

                </div>
            </div>

        </div>

    </div>


    <?php ActiveForm::end(); ?>

    <template v-if="showChangeImageModal" @close="showChangeImageModal = false">
        <div class="modal fade in" tabindex="-1" role="dialog" style="display: block;">
            <div class="modal-dialog" role="document">
                <div class="modal-content my-overflow-y">
                    <div class="modal-header">
                        <button type="button" @click="showChangeImageModal = false" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Замена картинки</h4>
                    </div>
                    <div class="modal-body">
                        <template v-for="(image, key, index) in changeImages">
                            <div class="col-sm-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="changeImageId" v-bind:value="image.id"
                                               v-model="pickedChangeImage">
                                        <img v-bind:src="image.src" alt="" class="img-responsive">
                                    </label>
                                </div>
                            </div>
                        </template>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"
                                @click="showChangeImageModal = false">Close
                        </button>
                        <button type="button" class="btn btn-primary" v-on:click.prevent="toChangeImage()">Save
                            changes
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </template>

</div>

<style>
    a.active[data-filter] span {
        background-color: green !important;
    }
</style>
<?php

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/cloudzoom.js', [
    'position' => \yii\web\View::POS_END
]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/cloudzoom.css', [
    'position' => \yii\web\View::POS_HEAD
]);

// <!-- dev version for console log -->
$this->registerJsFile('https://cdn.jsdelivr.net/npm/vue/dist/vue.js');

$script = <<< JS

var app = new Vue({
    el: '#app-container',
    data: {
        // getted from server
        product: [],
        productImg: [],
        countries: [],
        objects: [],
        objectsCountriesList: [],
        oldResults: [],
        statuses: [],
        aTypes: [],
        adModels: [],
        notifications: [],
        nextId: null,
        // no getted from server
        checkedObjects: [],
        filterCountry: null, // default
        pickedResult: null,
        pickedObject: null,
        isAutomated: false,
        showChangeImageModal: false,
        pickedChangeImage: null,
        changeImages: [], // images of product for changing
    },
    created: function() {
        this.getLostProduct($productId);
    },
    computed: {
        countChecked: function() {
            var count = 0;

            // it works for objects more than one
            // for (var prop in this.objects) {
            //   if( this.objects.hasOwnProperty( prop ) ) {
            //       if(this.objects[prop].checked) count++;
            //   } 
            // }

            // logic for one object
            count = ( this.pickedObject ) ? 1 : 0;

            CloudZoom.quickStart();

            return count;
        },
        isSetResultDisabled: function() {
            console.log('isSetResultDisabled', this.pickedResult, this.pickedObject);
            if(this.pickedResult > 0 && this.pickedObject) return false;
            return true;
        }
    },
    methods: {
        getStatusStr: function(status) {
            return this.statuses[status];
        },
        getTypeStr: function(type) {
            return this.aTypes[type];
        },
        getAdModelStr: function(model) {
            return this.adModels[model];
        },
        isEmpty(array){
            if(typeof array != "undefined" && array != null && array.length != null && array.length > 0){
                return false;
            } else {
                return true;
            }
        },
        is3Del: function(i) {
            return (i%3 == 0);
        },
        lostNoResult: function(productId, type, event) {
            console.log('lostNoResult',type, productId);
            var that = this;
            $.ajax({
                url: 'lost-no-result',
                type: 'post',
                data: {
                    productId: productId,
                    type: type
                },
                success: function (response) {
                    console.log('response',response);
                    that.notifications = response.notifications;

                    Vue.set(that.product, 'substitution_product_id', response['substitution_product_id']);
                    Vue.set(that.product, 'url', response['url']);

                    delete that;
                }
            });
        },
        filterTag: function(countryId, event) {
            console.log('filterTag');
            this.filterCountry = countryId;
        },
        resetFilter: function(event) {
            console.log('resetFilter');
            this.filterCountry = null;
        },
        isIssetCountry: function(arr) {
            if(this.isEmpty(arr) || !this.filterCountry) {
                return true;
            }

            for (var i = 0; i < arr.length; i++){
                if (arr[i][0] == this.filterCountry){
                    return true;
                }
            }

            return false;
        },
        filterResults(result){
            // result.countries
            if (!this.isIssetCountry(result.countries)){
                return false;
            } else {
                if(!this.pickedObject) return true;
                if( this.isEmpty(this.pickedObject.account_countries) || this.isEmpty(result.countries) ) return true;

                // if at least one intersection
                return result.countries.some(rCountryData => {return this.pickedObject.account_countries.some( oCountryData => {if(rCountryData[0] === oCountryData[0]) return true;} )});

            }
        },
        setResult: function() {
            console.log('setResult');
            var that = this;
            $.ajax({
                url: 'add-alternative-product',
                type: 'post',
                data: {
                    alternative: that.pickedResult,
                    pickedObject: that.pickedObject,
                    objects: that.objects,
                    product: that.product,
                    isAutomated: that.isAutomated
                },
                success: function (response) {
                    console.log('response',response);
                    // response.notifications = {};
                    if(response.notifications){
                        // that.notifications.forEach(function() {
                        //   that.notifications.pop();
                        // });
                        that.notifications.splice(0);
                        response.notifications.forEach(function(n, i) {
                            // Рекомендуем указывать key с v-for по возможности всегда, за исключением случаев, когда итерируемый контент 
                            // DOM прост, или когда вы сознательно используете стратегию по умолчанию для улучшения производительности.
                            n.key = i + Date.now();
                            that.notifications.push(n);
                        });
                    }
                    that.objects = that.objects.map( obj => {
                        if(obj && obj.object_id == response['substituted_object_id']) obj.is_substituted = true;
                    return obj;
                });
                    Vue.set(that.product, 'substitution_product_id', response['substitution_product_id']);
                    Vue.set(that.product, 'url', response['url']);
                    console.log('objects', that.objects);

                    delete that;
                },
                error: function() {
                    throw new Error('Network response was not ok');
                }
            });
        },
        toNext: function(oldId, nextId, event) {
            console.log(oldId, nextId, event);
            this.getLostProduct(nextId);
        },
        getLostProduct: function(productId) {
            // with fetch() does not worked on backend
            // with ajax work
            var that = this;
            $.ajax({
                url: 'lost-product',
                type: 'post',
                data: {
                    productId: productId,

                },
                success: function (response) {
                    console.log(response);
                    that.product = response.product;
                    that.productImg = response.productImg;
                    that.objectsCountriesList = response.objectsCountriesList;
                    that.objects = response.objects;
                    that.countries = response.countries;
                    that.oldResults = response.oldResults;
                    that.statuses = response.statuses;
                    that.aTypes = response.aTypes;
                    that.adModels = response.adModels;
                    that.nextId = response.nextId;

                    delete that; // improve speed
                },
                error: function() {
                    throw new Error('Network response was not ok');
                }
            });
        },
        changeImage: function(result) {
            var that = this;
            $.ajax({
                url: 'change-image',
                type: 'post',
                data: {
                    result: result
                },
                success: function(response) {
                    console.log(response, result);

                    that.changeImages = response.changeImages;
                    that.pickedResult = result.id;
                    that.showChangeImageModal = true; // show popup

                    delete that;
                }
            });

        },
        toChangeImage: function() {
            var that = this;
            $.ajax({
                url: 'to-change-image',
                type: 'post',
                data: {
                    pickedResult: that.pickedResult,
                    pickedChangeImage: that.pickedChangeImage
                },
                success: function(response) {
                    Vue.set(that.oldResults[that.pickedResult], 'picture_id', that.pickedChangeImage);
                    Vue.set(that.oldResults[that.pickedResult], 'img', response.newImg);
                    that.showChangeImageModal = false;

                    delete that;
                }
            });
        }

    },
    filters: {
        formatPrice: function(price, currency){
            let val = (price/1).toFixed(2).replace('.', ',');
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' ' + currency;
        }
    },
    // watch ...
    watch: {
    },
    // console log if mounted
    mounted: function () {
        console.log('mounted!');
    }
});


JS;

$this->registerJs($script, \yii\web\View::POS_READY);
?>


