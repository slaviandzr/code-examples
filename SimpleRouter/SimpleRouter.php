<?php
/**
 * Created by PhpStorm.
 * User: lukoyanov
 * Date: 25.06.18
 * Time: 11:46
 */


class SimpleRouter
{
    public static $routes = array();
    public static $url;
    public static $urlParams;
    public static $splittedPath;
    public static $controller;

    /**
     * processing url
     */
    public static function toRoute($requestedUrl = null) {
        if ($requestedUrl === null) {
            $uri = reset(explode('?', $_SERVER["REQUEST_URI"]));
            $requestedUrl = urldecode(rtrim($uri, '/'));
        }

        foreach (self::$routes as $route => $dispatch){

            if (strpos($route, ':') !== false) {
                $route = str_replace(':item:', '(.+)', str_replace(':id:', '([0-9]+)', $route));
                $route = str_replace(':params:', '(.+)', $route);
            }

            if (preg_match('#^'.$route.'$#', $requestedUrl)) {
                // first match

                list(self::$splittedPath, self::$urlParams) = self::splitUrl($requestedUrl);
                list($routeData, $routeParams) = self::splitUrl($dispatch);
//                self::pr([self::$splittedPath, self::$urlParams, $routeData]);
                self::checkComponentsExistion($routeData);

                return self::executeAction($routeData);

                break;
            }

        }

        throw new Exception('There are no any routes for your request. ');

    }

    public static function executeAction($routeData) {
        $controller = isset($routeData[0]) ? $routeData[0]: 'index';
        $action = isset($routeData[1]) ? $routeData[1]: 'index';
        $params = array_slice(self::$splittedPath, 2);
        $finishParams = [];
        if(!empty($params)) {
            $finishParams['item'] = $params[0] ?? null;
            $finishParams['id'] = $params[1] ?? null;
            if(self::$urlParams) $finishParams['params'] = self::$urlParams;
        }

        return call_user_func_array(array(
            self::$controller,
            $action
        ), $finishParams);
    }

    public static function checkComponentsExistion($splittedUrl){
        $className = ucfirst($splittedUrl[0]).'Controller';
        $namespace = 'controllers/'.ucfirst($splittedUrl[0]).'Controller';
        if (file_exists($namespace.'.php')){
            require_once($namespace . '.php');
            self::$controller = new $className();
            if(!method_exists(self::$controller,$splittedUrl[1])) throw new Exception('Method does not exist');
        } else {
            throw new Exception('Controller does not exist');
        }

        return true;
    }

    public function pr($obj){
        echo '<pre>';
        echo (var_dump($obj));
        echo '</pre>';
    }

    /**
     * @param $route
     * @param $dispatch
     * @throws \Exception
     */
    public static function addRoute($route, $dispatch){
        if(empty($route) || empty($dispatch)) throw new \Exception('Bad data in add route function. ');
        self::$routes = array_merge(self::$routes, [$route => $dispatch]);
    }

    public static function splitUrl($url) {
        $urlParseData = parse_url($url);
        if(empty($urlParseData['path'])) throw new Exception('Wrong request url. Empty path.');
        $urlParseDataArray = [];
        $params = (!empty($urlParseData['query'])) ? explode('&', $urlParseData['query']) : null;

        if($params){
            foreach ($params as $d){
                $e = explode('=', $d);
                if( !empty($e[0]) && !empty($e[1]) ) $urlParseDataArray[$e[0]] = $e[1];
            }
        }

        return [preg_split('/\//', $urlParseData['path'], -1, PREG_SPLIT_NO_EMPTY), $urlParseDataArray];
    }
}