<?php
/**
 * Created by PhpStorm.
 * User: lukoyanov
 * Date: 25.06.18
 * Time: 11:49
 */

class ItemsController
{
    /**
     * @param string $item
     * @param string $id
     * @return mixed
     */
    public function get($item, $id){
        echo 'get action Item controller. Params: ' . $item . ', ' . $id . PHP_EOL;
        return true;
    }

    /**
     * @param string $item
     * @param string $id
     * @param array $params
     * @return mixed
     */
    public function set($item, $id, $params){
        echo 'set action Item controller. Params: ' . $item . ', ' . $id . PHP_EOL;
        var_dump($params);
        return true;
    }
}