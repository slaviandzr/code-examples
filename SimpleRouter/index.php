<?php
/**
 * Created by PhpStorm.
 * User: lukoyanov
 * Date: 25.06.18
 * Time: 11:47
 */

spl_autoload_register(function ($class_name){
    include $class_name . '.php';
});

$router = new SimpleRouter();

try {
    $router::addRoute('/resource/action', 'resource/action');
    $router::addRoute('/items/get/:item:/:id:', 'items/get');
    $router::addRoute('/items/set/:item:/:id:?:params:', 'items/set');
    $router::toRoute('/items/set/yaya/1?test=1&test2=2&petia=5');
    $router::toRoute('/items/get/ooh/13321');
    $router::toRoute('/resource/action');
    $router::toRoute('/foo/bar');
} catch (Exception $e) {
    echo $e->getMessage();
}


