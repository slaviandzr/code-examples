var getCanvas;

var model = [
	["model", "Модель"],
	[1, "iPhone 6S", 47990],
	[0, "iPhone 7", 56990],
	[0, "iPhone 7 Plus", 67990]
];
var color = [
	["color", "Цвет корпуса"],
	[0, "Rose", 0],
	[0, "Gold", 0],
	[1, "Silver", 0],
	[0, "Space Grey", 0],
	[0, "Black", 0],
	[0, "Black Onyx", 0]
];
var memory = [
	["memory", "Объем памяти"],
	[1, "16 Gb", 0],
	[0, "32 Gb", 0],
	[0, "64 Gb", 9000],
	[0, "128 Gb", 9000],
	[0, "256 Gb", 18000]
];

var master = [
	["master", "Работа Ателье"],
	[1, "Промышленный дизайн, тестирование телефона, разборка и сборка корпуса, монтаж деталей, предпродажная подготовка", 45000]
];

var basePrice = 100000;
var curModelName = "iPhone SE";
var curStep = 1;

var base = [
	//id блока, Наименование блока, Иконка заголовка
	["base", "Основа телефона", "config/images/icons/smartphone-ico.png"],
	//[Текущее состояние, Состояние на прошлом шаге, Состояние по умолчанию, Название, цена, Изображние в теле левого блока, Изображение на визуализации iPhone]
	[0, 0, 0, "Пластина", "Пластина", "", "config/images/controls/_control_icon_base_type_plate_gold.png"],
	[0, 0, 0, "Рамка", "Рамка",  "", "config/images/controls/_control_icon_base_type_frame_gold.png"]
];
var plateMaterial = [
	["plateMaterial", "Материал основы", "config/images/icons/material-ico.png"],
	[0, 0, 0, "Золото", "Пластина золотая", 20000, "config/images/controls/_control_icon_base_type_plate_gold.png", "#img1_6s", "#img1_7", "img1_7plus"],
	[0, 0, 0, "Титан", "Пластина титановая", 10000, "config/images/controls/_control_icon_base_type_plate_ti.png", "#img2_6s", "#img2_7", "img2_7plus"]
];
var frameMaterial = [
	["frameMaterial", "Материал основы", "config/images/icons/material-ico.png"],
	[0, 0, 0, "Золото", "Рамка золотая", 12000, "config/images/controls/_control_icon_base_type_frame_gold.png", "#img20_6s", "#img20_7", "#img20_7plus"],
	[0, 0, 0, "Титан", "Рамка титановая", 8000, "config/images/controls/_control_icon_base_type_frame_ti.png", "#img21_6s", "#img21_7", "#img21_7plus"]
];
var plateGoldPattern = [ //Для материала пластины Золото
	["plateGoldPattern", "Узор пластины", "config/images/icons/pattern-ico.png"],
	[0, 0, 0, "Squares", "Узор пластины Squares", 14000, "config/images/controls/_control_icon_plate_pattern_squares_gold.png", "#img3_6s", "#img3_7", "#img3_7plus", 1, 0, 0],
	[0, 0, 0, "Wave", "Узор пластины Wave",  12000, "config/images/controls/_control_icon_plate_pattern_wave_gold.png", "#img4_6s", "#img4_7", "#img4_7plus", 1, 1, 1],
	[0, 0, 0, "Russo", "Узор пластины Russo",  18000, "config/images/controls/_control_icon_plate_pattern_russo_gold.png", "#img5_6s", "#img5_7", "#img5_7plus", 1, 1, 1],
	[0, 0, 0, "Свой узор", "Cвой узор пластины",  20000, "config/images/controls/_control_icon_plate_pattern_custom_gold.png", "#img30_6s", "#img30_7", "#img30_7plus", 1, 1, 1],
	[0, 0, 0, "Triangles", "Узор пластины Triangles",  18000, "config/images/controls/_control_icon_pattern_triangles_gold.png", "#img39_6s", "#img39_7", "#img39_7plus", 0, 1, 1]
];
var frameGoldPattern = [ //Для материала рамы Золото
	["frameGoldPattern", "Узор рамки", "config/images/icons/pattern-ico.png"],
	[0, 0, 0, "Squares", "Узор рамки Squares", 8000, "config/images/controls/_control_icon_frame_pattern_squares_gold.png", "#img22_6s", "#img22_7", "#img22_7plus", 1, 0, 0],
	[0, 0, 0, "Halal", "Узор рамки Halal",  9000, "config/images/controls/_control_icon_frame_pattern_halal_gold.png", "#img23_6s", "#img23_7", "#img23_7plus", 1, 0, 0],
	[0, 0, 0, "Russo", "Узор рамки Russo",  10000, "config/images/controls/_control_icon_frame_pattern_russo_gold.png", "#img24_6s", "#img24_7", "#img24_7plus", 1, 1, 1],
	[0, 0, 0, "Свой узор", "Свой узор рамки",  20000, "config/images/controls/_control_icon_frame_pattern_custom_gold.png", "#img20e_6s", "#img20e_7", "#img20e_7plus", 1, 1, 1],
	[0, 0, 0, "Triangles", "Узор рамки Triangles",  10000, "config/images/controls/_control_icon_pattern_triangles_gold.png", "#img37_6s", "#img37_7", "#img37_7plus", 0, 1, 1],
	[0, 0, 0, "Wave", "Узор рамки Wave",  10000, "config/images/controls/_control_icon_pattern_wave_gold.png", "#img38_6s", "#img38_7", "#img38_7plus", 0, 1, 1]
];
var plateTitanPattern = [ //Для материала пластины Титан
	["plateTitanPattern", "Узор пластины", "config/images/icons/pattern-ico.png"],
	[0, 0, 0, "Russia", "Узор пластины Russia титановая", 19000, "config/images/controls/_control_icon_plate_pattern_russia_ti.png", "#img29_6s", "#img29_7", "#img29_7plus", 1, 0, 0],
	[0, 0, 0, "Свой узор", "Свой узор пластины",  20000, "config/images/controls/_control_icon_plate_pattern_custom_ti.png", "#img31_6s", "#img31_7", "#img31_7plus", 1, 1, 1],
	[0, 0, 0, "Triangles", "Узор пластины Triangles",  18000, "config/images/controls/_control_icon_pattern_triangles_ti.png", "#img40_6s", "#img40_7", "#img40_7plus", 0, 1, 1],
	[0, 0, 0, "Wave", "Узор пластины Wave",  18000, "config/images/controls/_control_icon_pattern_wave_ti.png", "#img41_6s", "#img41_7", "#img41_7plus", 0, 1, 1],
	[0, 0, 0, "Russo", "Узор пластины Russo",  18000, "config/images/controls/_control_icon_pattern_russo_ti.png", "#img45_6s", "#img45_7", "#img45_7plus", 0, 1, 1]
];
var frameTitanPattern = [ //Для материала рамы Титан
	["frameTitanPattern", "Узор рамки", "config/images/icons/pattern-ico.png"],
	[0, 0, 0, "TiTouch - Cella", "Узор рамки TiTouch - Cella", 10000, "config/images/controls/_control_icon_frame_pattern_cella_ti.png", "#img25_6s", "#img25_7", "#img25_7plus", 1, 0, 0],
	[0, 0, 0, "Свой узор", "Свой узор рамки",  20000, "config/images/controls/_control_icon_frame_pattern_custom_ti.png", "#img21e_6s", "#img21e_7", "#img21e_7plus", 1, 1, 1],
	[0, 0, 0, "Triangles", "Узор рамки Triangles", 10000, "config/images/controls/_control_icon_pattern_triangles_ti.png", "#img42_6s", "#img42_7", "#img42_7plus", 0, 1, 1],
	[0, 0, 0, "Russo", "Узор рамки Russo", 10000, "config/images/controls/_control_icon_pattern_russo_ti.png", "#img43_6s", "#img43_7", "#img43_7plus", 0, 1, 1],
	[0, 0, 0, "Waves", "Узор рамки Waves", 10000, "config/images/controls/_control_icon_pattern_wave_ti.png", "#img44_6s", "#img44_7", "#img44_7plus", 0, 1, 1]
];
var centralElement = [
	["centralElement", "Центральный элемент", "config/images/icons/pattern-ico.png"],
	[0, 0, 0, "Кожа", "Кожа", "", "config/images/controls/_control_icon_central_type1.png", "", "", ""],
	[0, 0, 0, "Карбон", "Карбон", "", "config/images/controls/_control_icon_central_type2.png", "", "", ""],
	[0, 0, 0, "Дерево", "Дерево", "", "config/images/controls/_control_icon_central_type3.png", "", "", ""]
];
var leatherType = [ //Для центрального элемента "Кожа"
	["leatherType", "Тип кожи", "config/images/icons/pattern-ico.png"],
	[0, 0, 0, "Кожа аллигатора", "Центральный элемент 'Кожа аллигатора'", 15000, "config/images/controls/_control_icon_leather_type1.png", "#img14_6s", "#img14_7", "#img14_7plus"],
	[0, 0, 0, "Телячья кожа", "Центральный элемент 'Телячья кожа'", 14000, "config/images/controls/_control_icon_leather_type2.png", "#img15_6s", "#img15_7", "#img15_7plus"],
	[0, 0, 0, "Гладкая кожа", "Центральный элемент 'Гладкая кожа'", 12000, "config/images/controls/_control_icon_leather_type3.png", "#img16_6s", "#img16_7", "#img16_7plus"],
	[0, 0, 0, "Белая кожа питона", "Центральный элемент 'Белая кожа питона'", 16000, "config/images/controls/_control_icon_leather_type4.png", "#img17_6s", "#img17_7", "#img17_7plus"],
	[0, 0, 0, "Розовая кожа питона", "Центральный элемент 'Розовая кожа питона'", 18000, "config/images/controls/_control_icon_leather_type5.png", "#img18_6s", "#img18_7", "#img18_7plus"],
	[0, 0, 0, "Синяя кожа питона", "Центральный элемент 'Синяя кожа питона'", 22000, "config/images/controls/_control_icon_leather_type6.png", "#img19_6s", "#img19_7", "#img19_7plus"]

];
var carbonType = [ //Для центрального элемента "Карбон"
	["carbonType", "Тип карбона", "config/images/icons/pattern-ico.png"],
	[0, 0, 0, "Standard", "Центральный элемент карбон 'Standard'", 10000, "config/images/controls/_control_icon_central_type2.png", "#img26_6s", "#img26_7", "#img26_7plus"]
];
var woodType = [ //Для центрального элемента "Дерево"
	["woodType", "Тип дерева", "config/images/icons/pattern-ico.png"],
	[0, 0, 0, "Makore", "Центральный элемент дерево 'Makore'", 10000, "config/images/controls/_control_icon_central_type3.png", "#img27_6s", "#img27_7", "#img27_7plus"]
];
var logo = [
	["logo", "Логотип", "config/images/icons/logo-ico.png"],
	[0, 0, 0, "Caviar", "Логотип Caviar",  10000, "config/images/controls/_control_icon_logo_caviar.png", "#img13", "#img13", "#img13"],
	[0, 0, 0, "Mercedes", "Логотип Mercedes", 12000, "config/images/controls/_control_icon_logo_mercedes.png", "#img11", "#img11", "#img11"],
	[0, 0, 0, "Porsche", "Логотип Porsche", 15000, "config/images/controls/_control_icon_logo_porsche.png", "#img12", "#img12", "#img12"],
	[0, 0, 0, "Ferrari", "Логотип Ferrari", 15000, "config/images/controls/_control_icon_logo_ferrari.png", "#img_seal_ferrari", "#img_seal_ferrari", "#img_seal_ferrari"],
	[0, 0, 0, "Герб РФ", "Логотип Россия", 12000, "config/images/controls/_control_icon_logo_rfseal.png", "#img_seal_rf", "#img_seal_rf", "#img_seal_rf"],
	[0, 0, 0, "Medina", "Логотип Medina", 15000, "config/images/controls/_control_icon_logo_medina.png", "#img_seal_medina", "#img_seal_medina", "#img_seal_medina"],
	[0, 0, 0, "Свой логотип", "Свой логотип", 35000, "config/images/controls/_control_icon_logo_custom.png", "", "", ""]
];
var barelief = [
	["barelief", "Барельеф", "config/images/icons/barelief-ico.png"],
	[0, 0, 0, "Владимир Путин", "Барельеф В.В. Путин", 30000, "config/images/controls/_control_icon_barelief_putin.png", "#img28_putin", "#img28_putin", "#img28_putin"],
	[0, 0, 0, "Ваш барельеф", "Ваш барельеф", 50000, "config/images/controls/_control_icon_barelief.png", "#img28", "#img28", "#img28"]
];
var table = [
	["table", "Табличка", "config/images/icons/table-ico.png"],
	[0, 0, 0, "Standard", "Табличка Standard", 10000, "config/images/controls/_control_icon_plate_standard_gold.png", "#img6", "#img6", "#img6"],
	[0, 0, 0, "Square", "Табличка Square", 15000, "config/images/controls/_control_icon_plate_square_gold.png", "#img7", "#img7", "#img7"],
	[0, 0, 0, "Angular", "Табличка Angular", 20000, "config/images/controls/_control_icon_plate_angular_gold.png", "#img8", "#img8", "#img8"]
];
var diamonds = [
	["diamonds", "Бриллианты", "config/images/icons/diamond-ico.png"],
	[0, 0, 0, "Бриллианты", "Бриллианты", 6000, "config/images/controls/_control_icon_diamonds.png"]
];
var gerb = [
	["gerb", "Герб", "config/images/icons/seal-ico.png"],
	[0, 0, 0, "Герб России", "Герб России", 10000, "config/images/controls/_control_icon_coat_rf_gold.png", "#img32", "#img32", "#img32"],
	[0, 0, 0, "Свой герб", "Свой герб", 25000, "config/images/controls/_control_icon_coat_custom_gold.png", "", "", ""]
];
var engraving = [
	["engraving", "Гравировка", "config/images/icons/engraving-ico.png"],
	[0, 0, 0, "Лазерная гравировка", "Лазерная гравировка", 10000, "config/images/controls/_control_icon_edges_laser.png", "#img33", "#img34", "#img34"],
	[0, 0, 0, "Ювелирные вставки", "Ювелирные вставки", 30000, "config/images/controls/_control_icon_edges_inset.png", "#img35", "#img36", "#img36"]
];
var box = [
	["box", "Коробка", "config/images/icons/box-ico.png"],
	[1, 1, 1, "Standard", "Коробка Standard", 10000, "config/images/elements-box/_control_icon_box_standard.png"],
	[0, 0, 0, "Russia", "Коробка Russia", 15000, "config/images/elements-box/_control_icon_box_russia.png"],
	[0, 0, 0, "Motore", "Коробка Motore", 20000, "config/images/elements-box/_control_icon_box_motore.png"]
];
var cover = [
	["cover", "Чехол", "config/images/icons/case-ico.png"],
	[0, 0, 0, "Firenze", "Чехол Firenze", 24000, "config/images/elements-cases/_control_icon_case_firenze.png"],
	[0, 0, 0, "Carbon", "Чехол Carbon", 15000, "config/images/elements-cases/_control_icon_case_carbon.png"],
	[0, 0, 0, "Bianco", "Чехол Bianco", 15000, "config/images/elements-cases/_control_icon_case_bianco.png"],
	[0, 0, 0, "Magenta", "Чехол Magenta", 19000, "config/images/elements-cases/_control_icon_case_magenta.png"]
];

var mainParams = [base, plateMaterial, frameMaterial, plateGoldPattern, frameGoldPattern, plateTitanPattern, frameTitanPattern, centralElement, leatherType, carbonType, woodType, logo, barelief, table, diamonds, gerb, engraving, box, cover];
var costBaseArr = [model, color, memory]; //Массив элементов, определяющих базовую стоимость
var costElementsArr = [box, plateMaterial, frameMaterial, plateGoldPattern, frameGoldPattern, plateTitanPattern, frameTitanPattern, leatherType, carbonType, woodType, logo, barelief, table, diamonds, gerb, engraving, cover]; //Массив элементов, определяющих стоимость с обвесом

var tableTxt = ["", 1, 20, 0]; //Текст, номер шрифта, размер, номер пластины
var edgeTxt = ["", "", 1, 0]; //Текст, номер шрифта, номер лазерная / ювелирная

var blockVisRedo=[], bodyVisRedo=[], blockVisUndo=[], bodyVisUndo=[];

var modelX;
$(document).ready(function () {

	// custom JS by V.Lukoyanov Start

	// информационное окошко около цены на 3 шаге. Открытие и закрытие
	$("#totalCostTxtAfter").on('click hover', function(){
		$("#totalCostInfo").fadeIn(500);
	});

	$("#totalCostInfoClose").on('click', function(){
		$("#totalCostInfo").fadeOut(500);
	});

	//цели метрики
	$(".configOpen").on("click", function () {
		console.log('metrika config open');
		yaCounter38456575.reachGoal('konstruktor'); return true;
	});
	//отправка формы внизу лендинга реализован через аттрибут onclick
	//отправка данных с конструктора - в случае успешного аякс запроса

	// custom JS by V.Lukoyanov End

	//Заполняем шаг 2 названиями моделей и ценами
	for (i=1; i<model.length; i++){
		$(".step2modelName").eq(i-1).text(model[i][1]);
		var minPrice = dSpace(model[i][2] + Math.min(color[1][2], color[2][2], color[3][2], color[4][2], color[5][2], color[6][2]) + Math.min(memory[1][2], memory[2][2], memory[3][2], memory[4][2], memory[5][2]));
		//console.log(minPrice);
		$(".step2modelPrice").eq(i-1).text("от " + minPrice + ",-").show();
	}

	//Центрируем окно конфигуратора
	var cpWidth = $("#configPopup").width();
	var cpHeight = $("#configPopup").height();
	var shbWidth = $("#shadowBox").width();
	var shbHeight = $("#shadowBox").height();
	if (cpWidth < shbWidth) { //Центрируем по горизонтали
		$("#configPopup").css("left",(shbWidth - cpWidth)/2);
	} else {
		$("#configPopup").css("left","0");
	}
	if (cpHeight < shbHeight) { //Центрируем по вертикали
		$("#configPopup").css("top",(shbHeight - cpHeight)/2);
	} else {
		$("#configPopup").css("top","0");
	}



	// вычисляем разность ширин относительно 1440px и нашего экрана
	var configPopup = $('#configPopup');

	var configPopupWidth = $(configPopup).width();
	var windowWidth = $(window).width();
	var configPopupHeight = $(configPopup).height();
	var windowHeight = $(window).height();
	var initialScaleX = windowWidth / configPopupWidth;
	var initialScaleY = windowHeight / configPopupHeight;
	var alignX = false, alignY = false;

	function getDimensionsOfConstructorRealTime(){
		//configPopupWidth = $(configPopup).getBoundingClientRect().width();
		configPopupWidth = document.getElementById("configPopup").getBoundingClientRect().width;
		windowWidth = $(window).width();
		//configPopupHeight = $(configPopup).getBoundingClientRect().height();
		configPopupHeight = document.getElementById("configPopup").getBoundingClientRect().height;
		windowHeight = $(window).height();
		initialScaleX = windowWidth / configPopupWidth;
		initialScaleY = windowHeight / configPopupHeight;
	}

	//Показываем окно конфигуратора при нажатии кнопки
	$(".configOpen").on("click", function () {

		$("#shadowBox").show();

		if ( initialScaleX > 1 && initialScaleY > 1 ){
			// все хорошо
			console.log('все хорошо');
		} else if( initialScaleX < initialScaleY ){
			// нужно подстроить popup 100% по ширине и выровнять по высоте
			$(configPopup).css({
				'left' : 0,
				'top' : 0,
				'transform-origin' : '0 0 0'
			});

			$(configPopup).css({
				'transform' : 'scale(' + initialScaleX + ')'
			});

			getDimensionsOfConstructorRealTime();
			//console.log('нужно подстроить popup 100% по ширине и выровнять по высоте!');
			alignY = true;
		} else {
			// нужно подстроить popup 100% по высоте и выровнять по ширине
			$(configPopup).css({
				'left' : 0,
				'top' : 0,
				'transform-origin' : '0 0 0'
			});

			$(configPopup).css({
				'transform' : 'scale(' + initialScaleY + ')'
			});

			getDimensionsOfConstructorRealTime();
			//console.log('нужно подстроить popup 100% по высоте и выровнять по ширине!');
			alignX = true;
		}

		$(configPopup).show("scale", 500, function(){
			var configWidth = document.getElementById("configPopup").getBoundingClientRect().width;
			var configHeight = document.getElementById("configPopup").getBoundingClientRect().height;
			var windowWidth = $(window).width();
			var windowHeight = $(window).height();
			if ( alignX ){
				$(configPopup).css({
					'left' : Math.floor( Math.abs(configWidth - windowWidth) / 2 ) + 'px'
				});
				//console.log('width = ' + document.getElementById("configPopup").getBoundingClientRect().width);
			}
			if ( alignY ){
				$(configPopup).css({
					'top' : Math.floor( Math.abs(configHeight - windowHeight) / 2 ) + 'px'
				});
				//console.log('width = ' + document.getElementById("configPopup").getBoundingClientRect().width);
			}
		});

	});

	//Закрываем окно конфигуратора при нажатии крестика или шадоубокса
	$("#closeBtn").on("click", function () {
		$("#shadowBox, #configPopup").hide("puff", 500);
	});

	//Переходим к Шагу 2
	$("#step1btn").on("click", function () {
		curStep = 2;
		$("#configStep1").hide("drop", {direction: "left"}, 500);
		$(".step2model").eq(0).animate({opacity: 1}, 200).show("drop", {direction: "right"}, 500);
		$(".step2model").eq(1).animate({opacity: 1}, 400).show("drop", {direction: "right"}, 500);
		$(".step2model").eq(2).animate({opacity: 1}, 600).show("drop", {direction: "right"}, 500);
		$("#step2head").animate({opacity: 1}, 1000).fadeIn(400);
	});

	//Создаем полосу прокрутки для перечня выбранных опций

	$("#selOptions").niceScroll({
		cursorcolor:"#8b8b8b",
		cursorwidth: "4px",
		cursorborder: "0",
		cursorborderradius: "2px",
		zindex: "1005",
		hidecursordelay: "1200",
		autohidemode: true,
		boxzoom:false
	});

	//Выбор модели
	$(".step2model").on("mouseenter", function () {
		$(this).addClass("mHover");
		$(".step2model:not(.mHover)").animate({opacity: 0.5}, 100); //Делаем полупрозрачными объекты не в фокусе
	});
	$(".step2model").on("mouseleave", function () {
		$(".step2model").removeClass("mHover").animate({opacity: 1}, 100);
	});


	//Модель выбрана
	$(".step2model").on("click", function () {
		if (curStep == 2) {
			//Присваиваем выбранной модели класс mSelected
			$(".step2model").addClass("mNotSelected").removeClass("mSelected");
			$(this).removeClass("mNotSelected").addClass("mSelected");
			//Ставим флаг на выбранной модели
			$.each($(".step2model"), function(i, val) {
				if ($(this).hasClass("mSelected")) {
					setFlag (model, i+1);
				}
			});
			curModelName = curOption(model)[1] + " " + curOption(color)[1] + " " + curOption(memory)[1];
			$("#curModel").text(curModelName);

			//Переходим к шагу 3
			curStep = 3;
			$(".mNotSelected, .mSelected .step2modelPrice, .mSelected .step2modelName").fadeOut();
			$("#configStep3").show("slide", {direction: "right"}, 500);
			$("#curModelContainer").fadeIn(500);
			modelX = $(".mSelected").css("left");
			$(".mSelected").animate({left: "450px"}, 500);
			$(".colorBox").eq(0).animate({opacity: "1"}, 400).show("puff", 300);
			$(".colorBox").eq(1).animate({opacity: "1"}, 500).show("puff", 300);
			$(".colorBox").eq(2).animate({opacity: "1"}, 600).show("puff", 300);
			if (model[1][0] == 1) { //Если выбран 6S
				$(".colorBox").eq(3).animate({opacity: "1"}, 700).show("puff", 300);
				$(".memoryBox").removeClass("mbSelected");
				$(".memoryBox").eq(0).animate({opacity: "1"}, 800).show("puff", 400).addClass('mbSelected');
				setFlag (memory, 1);
				$(".memoryBox").eq(2).animate({opacity: "1"}, 1000).show("puff", 400);
				$(".memoryBox").eq(3).animate({opacity: "1"}, 1200).show("puff", 400);

			} else {
				//iphone 7 & 7 Plus
				$(".colorBox").eq(4).animate({opacity: "1"}, 700).show("puff", 300);
				$(".colorBox").eq(5).animate({opacity: "1"}, 800).show("puff", 300);
				$(".memoryBox").removeClass("mbSelected");
				$(".memoryBox").eq(1).animate({opacity: "1"}, 800).show("puff", 400).addClass('mbSelected');
				setFlag (memory, 2);
				$(".memoryBox").eq(3).animate({opacity: "1"}, 1000).show("puff", 400);
				$(".memoryBox").eq(4).animate({opacity: "1"}, 1200).show("puff", 400);
			}
			curModelName = curOption(model)[1] + " " + curOption(color)[1] + " " + curOption(memory)[1];
			$("#curModel").text(curModelName);
			$("#step3priceVal").text(dSpace(calcBasePrice()) + ",-");
			$("#step3btn").fadeIn(500);
		}
	});

	//Возвращаемся к выбору модели
	$("#backToStep2").on("click", function () {
		curStep = 2;
		// по умолчанию делаем все модели серебристыми
		changeSrc(["config/images/elements-6s/_i6s_base_silver.jpg", "config/images/elements-7/_i7_base_silver.jpg", "config/images/elements-7/_i7plus_base_silver.jpg"]);
		$(".colorBox").removeClass("cbSelected");
		$(".colorBox").eq(2).addClass("cbSelected");
		setFlag (color, 3);

		$(".memoryBox").removeClass("mbSelected");
		$(".memoryBox").eq(0).addClass("mbSelected"); 
		setFlag (memory, 1); //Ставим отметку на 16Гб
		$(".mNotSelected, .mSelected .step2modelPrice, .mSelected .step2modelName").fadeIn();
		$("#configStep3").hide("slide", {direction: "right"}, 500);
		$("#curModelContainer, #step3btn, .colorBox, .memoryBox").fadeOut(500);
		$(".mSelected").animate({left: modelX}, 500);
	});


	//Выбор цвета
	$(".colorBox").on("click", function () {
		$(".colorBox").removeClass("cbSelected");
		$(this).addClass("cbSelected");

		//Ставим флаг на выбранном цвете
		$.each($(".colorBox"), function(i, val) {
			if ($(this).hasClass("cbSelected")) {
				setFlag (color, i+1);
				$("#step3priceVal").text(dSpace(calcBasePrice()) + ",-");
			}
		});
		curModelName = curOption(model)[1] + " " + curOption(color)[1] + " " + curOption(memory)[1];
		$("#curModel").text(curModelName);
		//Меняем изображение модели
		if (color[1][0] == 1) { //Rose
			changeSrc(["config/images/elements-6s/_i6s_base_rosegold.jpg", "config/images/elements-7/_i7_base_rosegold.jpg", "config/images/elements-7/_i7plus_base_rosegold.jpg"]);
		} else if (color[2][0] == 1) { //Gold
			changeSrc(["config/images/elements-6s/_i6s_base_gold.jpg", "config/images/elements-7/_i7_base_gold.jpg", "config/images/elements-7/_i7plus_base_gold.jpg"]);
		} else if (color[3][0] == 1) { //Silver
			changeSrc(["config/images/elements-6s/_i6s_base_silver.jpg", "config/images/elements-7/_i7_base_silver.jpg", "config/images/elements-7/_i7plus_base_silver.jpg"]);
		} else if (color[4][0] == 1) { //Gray
			changeSrc(["config/images/elements-6s/_i6s_base_spacegray.jpg", "", ""]);
		} else  if (color[5][0] == 1){ //Black
			changeSrc(["", "config/images/elements-7/_i7_base_black.jpg", "config/images/elements-7/_i7plus_base_black.jpg"]);
		} else  if (color[6][0] == 1){ //Black Onyx
			changeSrc(["", "config/images/elements-7/_i7_base_jetblack.jpg", "config/images/elements-7/_i7plus_base_jetblack.jpg"]);
		}

		// если модель не 6S и выбранный цвет не оникс, то показываем 32 Гб
		if ( model[1][0] != 1 && color[6][0] != 1 ) {
			$('#32gb').show('500');
		}

		// если выбрали оникс, то убираем 32 Гб
		// если установлен 32 Гб, то ставим 128 Гб
		if (color[6][0] == 1){
			$('#32gb').hide('500');
			// если память была 32Гб, то имитируем клик на 128Гб
			if (memory[2][0] == 1){
				$('#128gb').click();
			}
		}
	});

	//Выбор памяти
	$(".memoryBox").on("click", function () {
		$(".memoryBox").removeClass("mbSelected");
		$(this).addClass("mbSelected");
		//Ставим флаг на выбранном цвете
		$.each($(".memoryBox"), function(i, val) {
			if ($(this).hasClass("mbSelected")) {
				setFlag (memory, i+1);
				$("#step3priceVal").text(dSpace(calcBasePrice()) + ",-");
			}
		});
		curModelName = curOption(model)[1] + " " + curOption(color)[1] + " " + curOption(memory)[1];
		$("#curModel").text(curModelName);
	});


	//Заполняем Шаг 4 блоками параметров
	var optBlock = "", optCost = 0;
	for (i=0; i<mainParams.length; i++) {
		optBlock += "<div class='step4block' id='" + mainParams[i][0][0] + "'>";
		optBlock += "<div class='step4lbHead'><div class='lineBtm'></div><div class='step4lbhIco'><img src='" + mainParams[i][0][2] + "'></div><div class='step4lbhTxt'>" + mainParams[i][0][1] + "</div><div class='step4lbhDown'></div></div>";
		optBlock += "<div class='step4lbBody dNone'>";
		for (j=1; j<mainParams[i].length; j++) {

			optCost = mainParams[i][j][5];
			if (typeof(optCost) == "number") {
				optCost = dSpace(optCost) + ",-";
			}

			optBlock += "<div class='step4lbbElement  el-" + (mainParams[i].length - 1) + "' data-param='" + i + "%" + j + "'><img src='" + mainParams[i][j][6] + "'><p class='step4elName'>" + mainParams[i][j][3] + "</p><p class='step4elPrice'>" + optCost + "</p></div>"
		}
		optBlock += "<div class='cBoth'></div></div></div>"
	}
	$("#step4left").append(optBlock);
	//Раскрываем блок выбора основы телефона
	$("#base .step4lbBody").removeClass("dNone");
	// Добавляем значки редактирования текста таблички и гравировки
	$("#table .step4lbHead, #engraving .step4lbHead").append("<div class='step4lbhEdit'></div>");
	//Скрываем недоступные опции
	$("#plateMaterial, #frameMaterial, #plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide();

	// функция, которая показывает/скрывает нужные/ненужные блоки параметров в зависимости от выбранной модели телефона
	function hideParamsBlocksforPhone(){
		for (i=0; i<mainParams.length; i++) {
			for (j=1; j<mainParams[i].length; j++) {

				// наш суперкостыль для показа/скрытия нужных иконок
				// в зависимости от текущей модели телефона
				var blockElement = $('#' + mainParams[i][0][0]).find(".step4lbbElement[data-param='" + i + "%" + j + "']");
				console.log(blockElement.length);
				if (model[1][0] == 1) { //6S
					if ( mainParams[i][j][10] == 0 ) {
						$(blockElement).hide();
					} else {
						$(blockElement).show();
					}
				} else if(model[2][0] == 1){ // iPhone 7
					if ( mainParams[i][j][11] == 0 ) {
						$(blockElement).hide();
					} else {
						$(blockElement).show();
					}
				} else if(model[3][0] == 1) {// 7 Plus
					if ( mainParams[i][j][12] == 0 ) {
						$(blockElement).hide();
					} else {
						$(blockElement).show();
					}
				}

			}
		}
	}

	//Переходим к шагу 4
	$("#step3btn").on("click", function () {

		curStep = 4;
		$("#configStep2").fadeOut(500);
		$(".mSelected").animate({left: "487px", top: "177px", width: "447px", height: "547px"}, 800);
		$(".mSelected img").animate({width: "447px", height: "547px"}, 800);
		$("#step4main").animate({opacity: 1}, 600).fadeIn(800);
		$("#step4left").animate({opacity: 1}, 600).show("drop", {direction: "left"}, 800);
		$("#step4right").animate({opacity: 1}, 600).show("drop", {direction: "right"}, 800);
		render();
		$("#engraving .step4lbbElement").eq(1).show(); //Показываем ювелирные вставки на случай если раньше был выбран SE

		hideParamsBlocksforPhone();

	});

// При клике на "Выбрать другую модель" возвращаемся на шаг 3
	$("#selOtionsConainer").on("click", "#backToStep3", function () {
		curStep = 3;
		clearFlag(mainParams); //Сбрасываем выбранные параметры
		render();		
		$("#step4right").hide("drop", {direction: "right"}, 800);
		$("#step4left").hide("drop", {direction: "left"}, 800);
		$("#step4main").fadeOut(800);
		if (model[1][0] == 1) { //6S
			$(".mSelected img").animate({width: "259px", height: "320px"}, 800);
			$(".mSelected").animate({left: "450px", top: "265px", width: "160px", height: "392px"}, 800);
		} else if(model[2][0] == 1){ // iPhone 7
			$(".mSelected img").animate({width: "286px", height: "350px"}, 800);
			$(".mSelected").animate({left: "450px", top: "235px", width: "174px", height: "457px"}, 800);		
		} else {// 7 Plus
			$(".mSelected img").animate({width: "315px", height: "385px"}, 800);
			$(".mSelected").animate({left: "450px", top: "200px", width: "200px", height: "457px"}, 800);

		}
		$("#configStep2").fadeIn(500);		
		$("#plateMaterial, #frameMaterial, #plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide(); //Прячем недоступные опции
		spoilerShow("base");
		$("#acbUndo, #acbRedo").addClass("acbDisable");
	});



// При клике по заголовку опции разворачиваем/сворачиваем ее содержимое
	$(".step4lbHead").on("click", function (e) {
		if ($(e.target).attr("class") != "step4lbhEdit") {
			if ($(this).parent().find(".step4lbBody").css("display") == "none") {
				$(".step4lbBody").hide("blind", 300);
				$(".step4lbhDown").removeClass("rotate180");
				$(this).find(".step4lbhDown").addClass("rotate180");
				$(this).parent().find(".step4lbBody").show("blind", 300);
				$("#step4left").animate({opacity: 1}, 400).niceScroll({
					cursorcolor:"#8b8b8b",
					cursorwidth: "4px",
					cursorborder: "0",
					cursorborderradius: "2px",
					zindex: "1005",
					hidecursordelay: "1200",
					autohidemode: true,
					boxzoom:false
				});
			}  else {
				$(this).parent().find(".step4lbBody").hide("blind", 300);
				$(this).find(".step4lbhDown").removeClass("rotate180");
			}
		}
	});
//При клике по опции
	$(".step4lbbElement").on("click", function () {
		//Записываем текущую видимость опций
		blockVisUndo = [];
		bodyVisUndo = [];
		$.each($(".step4block"), function () {
			blockVisUndo.push($(this).css("display"));
		});
		$.each($(".step4lbBody"), function () {
			bodyVisUndo.push($(this).css("display"));
		});
		//Записываем текущие флаги опций
		undoWrite();
		//Устанавливаем флаг на кликнутой опции
		if ($(this).attr("data-param")) {
			var paramArr = $(this).attr("data-param").split("%");
			var paramI = parseInt(paramArr[0]);
			var paramJ = parseInt(paramArr[1]);
			setFlag(mainParams[paramI], paramJ);
			render();
		}
	});

//При выборе основы телефона "Пластина"
	$("#base .step4lbbElement").eq(0).on("click", function () {
		$("#plateMaterial, #frameMaterial, #plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide();
		clearFlag([frameMaterial, frameGoldPattern, frameTitanPattern, centralElement, leatherType, carbonType, woodType]);
		render();
		$("#base .step4lbhDown").removeClass("rotate180");
		$("#plateMaterial").show("slide", 300).queue(function () {
			spoilerShow("plateMaterial");
			$(this).dequeue();
		});

	});
//При выборе материала основы пластины "Золото"
	$("#plateMaterial .step4lbbElement").eq(0).on("click", function () {
		$("#plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide();
		$("#plateTitanPattern").hide();
		clearFlag([plateTitanPattern]);
		render();
		$("#plateMaterial .step4lbhDown").removeClass("rotate180");
		if (model[1][0] == 1) { //Для SE не показываем чехлы и ювелирные вставки
			$("#engraving .step4lbbElement").eq(1).hide();
			$("#plateGoldPattern, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box").show("slide", 300).queue(function () {
				spoilerShow("plateGoldPattern");
				$(this).dequeue();
			});
		} else {
			$("#plateGoldPattern, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").show("slide", 300).queue(function () {
				spoilerShow("plateGoldPattern");
				$(this).dequeue();
			});
		}
	});
//При выборе материала основы пластины "Титан"
	$("#plateMaterial .step4lbbElement").eq(1).on("click", function () {
		$("#plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide();

		clearFlag([plateGoldPattern]);
		render();
		$("#plateMaterial .step4lbhDown").removeClass("rotate180");
		if (model[1][0] == 1) { //Для SE не показываем чехлы и ювелирные вставки
			$("#engraving .step4lbbElement").eq(1).hide();
			$("#plateTitanPattern, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box").show("slide", 300).queue(function () {
				spoilerShow("plateTitanPattern");
				$(this).dequeue();
			});
		} else {
			$("#plateTitanPattern, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").show("slide", 300).queue(function () {
				spoilerShow("plateTitanPattern");
				$(this).dequeue();
			});

		}



	});

//При выборе основы телефона "Рамка"
	$("#base .step4lbbElement").eq(1).on("click", function () {
		$("#plateMaterial, #frameMaterial, #plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide();
		clearFlag([plateMaterial, plateGoldPattern, plateTitanPattern]);
		render();
		$("#base .step4lbhDown").removeClass("rotate180");
		$("#frameMaterial").show("slide", 300).queue(function () {
			spoilerShow("frameMaterial");
			$(this).dequeue();
		});

	});
//При выборе материала основы рамки "Золото"
	$("#frameMaterial .step4lbbElement").eq(0).on("click", function () {
		$("#plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide();
		$("#frameTitanPattern").hide();

		clearFlag([frameTitanPattern]);
		render();
		$("#frameMaterial .step4lbhDown").removeClass("rotate180");
		if (model[1][0] == 1) { //Для SE не показываем чехлы и ювелирные вставки
			$("#engraving .step4lbbElement").eq(1).hide();
			$("#frameGoldPattern, #centralElement, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box").show("slide", 300).queue(function () {
				spoilerShow("frameGoldPattern");
				$(this).dequeue();
			});
		} else {
			$("#frameGoldPattern, #centralElement, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").show("slide", 300).queue(function () {
				spoilerShow("frameGoldPattern");
				$(this).dequeue();
			});
		}
	});
//При выборе материала основы рамки "Титан"
	$("#frameMaterial .step4lbbElement").eq(1).on("click", function () {
		$("#plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide();

		clearFlag([frameGoldPattern]);
		render();
		$("#frameMaterial .step4lbhDown").removeClass("rotate180");

		if (model[1][0] == 1) { //Для SE не показываем чехлы и ювелирные вставки
			$("#engraving .step4lbbElement").eq(1).hide();
			$("#frameTitanPattern, #centralElement, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box").show("slide", 300).queue(function () {
				spoilerShow("frameTitanPattern");
				$(this).dequeue();
			});
		} else {
			$("#frameTitanPattern, #centralElement, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").show("slide", 300).queue(function () {
				spoilerShow("frameTitanPattern");
				$(this).dequeue();
			});
		}
	});


//При выборе материала центрального элемента "Кожа"
	$("#centralElement .step4lbbElement").eq(0).on("click", function () {
		$("#carbonType, #woodType").hide();

		clearFlag([carbonType, woodType]);
		render();
		$("#centralElement .step4lbhDown").removeClass("rotate180");
		$("#leatherType").show("blind", 300).queue(function () {
			spoilerShow("leatherType");
			$(this).dequeue();
		});
	});
//При выборе материала центрального элемента "Карбон"
	$("#centralElement .step4lbbElement").eq(1).on("click", function () {
		$("#leatherType, #woodType").hide();

		clearFlag([leatherType, woodType]);
		render();
		$("#centralElement .step4lbhDown").removeClass("rotate180");
		$("#carbonType").show("blind", 300).queue(function () {
			spoilerShow("carbonType");
			$(this).dequeue();
		});
	});
//При выборе материала центрального элемента "Дерево"
	$("#centralElement .step4lbbElement").eq(2).on("click", function () {
		$("#carbonType, #leatherType").hide();

		clearFlag([carbonType, leatherType]);
		render();
		$("#centralElement .step4lbhDown").removeClass("rotate180");
		$("#woodType").show("blind", 300).queue(function () {
			spoilerShow("woodType");
			$(this).dequeue();
		});
	});
//При выборе подтипа материала центрального элемента
	$("#leatherType .step4lbbElement, #carbonType .step4lbbElement, #woodType .step4lbbElement").on("click", function () {
		if (model[1][0] == 1) { //Для SE не показываем чехлы
			$("#logo, #barelief, #table, #diamonds, #gerb, #engraving, #box").show("slide", 300);
		} else {
			$("#logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").show("slide", 300);
		}
	});

//При выборе логотипа
	$("#logo .step4lbbElement").on("click", function () {
		clearFlag([barelief]);
		render();
	});

//При выборе барильефа
	$("#barelief .step4lbbElement").on("click", function () {
		clearFlag([logo]);
		render();
	});



//Удаляем параметр при клике на крестик в спецификации
	$("#selOptions").on("click", ".optDel", function () {
		var paramArr = $(".optDel:hover").attr("data-param").split("%");
		var i = parseInt(paramArr[0]);
		var j = parseInt(paramArr[1]);
		undoWrite();
		costElementsArr[i][j][0]=0;

		//Прячем значки редактирования и удаляем тексты табличек и гравировки при удалении опции
		if (costElementsArr[i][0][0] == "table") {
			$("#table .step4lbhEdit").hide();
			tableTxt = ["", 1, 20, 0];
		} else if (costElementsArr[i][0][0] == "engraving") {
			$("#engraving .step4lbhEdit").hide();
			edgeTxt = ["", "", 1, 0];
		}

		render();
	});

	//Удаляем выбранные опции
	$("#acbClear").on("click", function () {
		$("#plateMaterial, #frameMaterial, #plateGoldPattern, #frameGoldPattern, #plateTitanPattern, #frameTitanPattern, #centralElement, #leatherType, #carbonType, #woodType, #logo, #barelief, #table, #diamonds, #gerb, #engraving, #box, #cover").hide();
		spoilerShow("base");
		clearFlag(mainParams);
		render();
	});

	//Шаг назад
	$("#acbUndo").on("click", undo);

	//Шаг вперед
	$("#acbRedo").on("click", redo);

	//Стилизуем селекты окон персонализации
	$("#tablePopup select").selectric();
	$("#edgePopup select").selectric();
	//Вызываем окно персонализации таблички
	$("#table .step4lbhEdit").on("click", tPopupOpen);
	$("#table .step4lbbElement").on("click", function () {
		tableTxt[3] = $("#table .step4lbbElement").index(this);
		if (tableTxt[0] == "") {
			tPopupOpen();
		}
	});
	function tPopupOpen() {
		//Отображаем нужную картинку
		$(".tPopupImg").hide();
		$(".tPopupImg").eq(tableTxt[3]).show();
		//Отображаем текущие значения
		$("#tPopupTxt").html(tableTxt[0]);
		$("#tPopupTxtInp").val(tableTxt[0].replace(RegExp("<br>","g"), "\n"));

		$("#tPopupFName option[value=" + tableTxt[1] + "]").prop("selected", true);
		switch (tableTxt[1]) {
			case "1":
				$("#tPopupFNameContainer .label, #tPopupTxt").css("font-family", "Gothic");
				break;
			case "2" :
				$("#tPopupFNameContainer .label, #tPopupTxt").css("font-family", "Goodvibes");
				break;
			case "3":
				$("#tPopupFNameContainer .label, #tPopupTxt").css("font-family", "Ceremonious");
				break;
		}
		$("#tPopupFSize option[value=" + tableTxt[2] + "]").prop("selected", true);
		$("#tPopupTxt").css("font-size", tableTxt[2] + "px");
		//Центрируем окно
		var tpWidth = $("#tablePopup").width();
		var tpHeight = $("#tablePopup").height();
		var shb1Width = $("#shadowBox1").width();
		var shb1Height = $("#shadowBox1").height();
		$("#tablePopup").css("left",(shb1Width - tpWidth)/2);
		$("#tablePopup").css("top",(shb1Height - tpHeight)/2);
		$("#shadowBox1").fadeIn(500);
		$("#tablePopup").show("scale", 500);
	}
	//Меняем шрифты
	$("#tPopupFName").on("change", function () {

		switch ($(this).find("option:selected").val()) {
			case "1":
				$("#tPopupFNameContainer .label, #tPopupTxt").css("font-family", "Gothic");
				break;
			case "2" :
				$("#tPopupFNameContainer .label, #tPopupTxt").css("font-family", "Goodvibes");
				break;
			case "3":
				$("#tPopupFNameContainer .label, #tPopupTxt").css("font-family", "Ceremonious");
				break;
		}
	});
	//Меняем размер шрифта
	$("#tPopupFSize").on("change", function () {
		$("#tPopupTxt").css("font-size", $(this).find("option:selected").val() + "px");
	});
	//Меняем надпись при вводе
	$("#tPopupTxtInp").on("keyup", function () {
		$("#tPopupTxt").html($("#tPopupTxtInp").val().replace(RegExp("\n","g"), "<br>"));
	});
	//Завершаем ввод данных таблички
	$("#tPopupBtn").on("click", function () {

		tableTxt[0] = $("#tPopupTxtInp").val().replace(RegExp("\n","g"), "<br>");
		tableTxt[1] = $("#tPopupFName option:selected").val();
		tableTxt[2] = $("#tPopupFSize option:selected").val();

		$(".tableTxt").html(tableTxt[0]);
		$(".tableTxt").css("font-size", tableTxt[2] + "px");

		switch (tableTxt[1]) {
			case "1":
				$(".tableTxt").css("font-family", "Gothic");
				break;
			case "2" :
				$(".tableTxt").css("font-family", "Goodvibes");
				break;
			case "3":
				$(".tableTxt").css("font-family", "Ceremonious");
				break;
		}
		$("#table .step4lbhEdit").show();
		$("#shadowBox1").hide();
		$("#tablePopup").hide("puff", 500);
	});
	//Закрываем окно персонализации таблички без изменений
	$("#tPopupClose").on("click", function () {
		$("#table .step4lbhEdit").show();
		$("#shadowBox1").fadeOut(500);
		$("#tablePopup").hide("puff", 500);
	});
	//Вызываем окно гравировки
	$("#engraving .step4lbhEdit").on("click", ePopupOpen);
	$("#engraving .step4lbbElement").on("click", function () {
		edgeTxt[3] = $("#engraving .step4lbbElement").index(this);
		if (edgeTxt[0] == "" && edgeTxt[1] == "") {
			ePopupOpen();
		}
	});
	function ePopupOpen() {
		//Отображаем нужную картинку
		$(".ePopupImg").hide();
		if (curOptionIndex(model) == 1) { //iPhoneSE
			$(".ePopupImg").eq(0).show();
		} else {
			if (edgeTxt[3] == 0) {
				$(".ePopupImg").eq(1).show();
			} else {
				$(".ePopupImg").eq(2).show();
			}
		}
		//Отображаем текущие значения
		$("#ePopupTxt1").html(edgeTxt[0]);
		$("#ePopupTxt2").html(edgeTxt[1]);
		$("#ePopupTxt1Inp").val(edgeTxt[0].replace(RegExp("<br>","g"), "\n"));
		$("#ePopupTxt2Inp").val(edgeTxt[1].replace(RegExp("<br>","g"), "\n"));

		$("#ePopupFName option[value=" + edgeTxt[2] + "]").prop("selected", true);
		switch (edgeTxt[2]) {
			case "1":
				$("#ePopupFNameContainer .label, #ePopupTxt1, #ePopupTxt2").css("font-family", "Gothic");
				break;
			case "2" :
				$("#ePopupFNameContainer .label, #ePopupTxt1, #ePopupTxt2").css("font-family", "Goodvibes");
				break;
			case "3":
				$("#ePopupFNameContainer .label, #ePopupTxt1, #ePopupTxt2").css("font-family", "Ceremonious");
				break;
		}
		var epWidth = $("#edgePopup").width();
		var epHeight = $("#edgePopup").height();
		var shb1Width = $("#shadowBox1").width();
		var shb1Height = $("#shadowBox1").height();
		$("#edgePopup").css("left",(shb1Width - epWidth)/2);
		$("#edgePopup").css("top",(shb1Height - epHeight)/2);
		$("#shadowBox1").fadeIn(500);
		$("#edgePopup").show("scale", 500);
	}
	//Меняем шрифты
	$("#ePopupFName").on("change", function () {

		switch ($(this).find("option:selected").val()) {
			case "1":
				$("#ePopupFNameContainer .label, #ePopupTxt1, #ePopupTxt2").css("font-family", "Gothic");
				break;
			case "2" :
				$("#ePopupFNameContainer .label, #ePopupTxt1, #ePopupTxt2").css("font-family", "Goodvibes");
				break;
			case "3":
				$("#ePopupFNameContainer .label, #ePopupTxt1, #ePopupTxt2").css("font-family", "Ceremonious");
				break;
		}
	});

	//Меняем надписи при вводе
	$("#ePopupTxt1Inp").on("keyup", function () {
		$("#ePopupTxt1").html($("#ePopupTxt1Inp").val().replace(RegExp("\n","g"), "<br>"));
	});
	$("#ePopupTxt2Inp").on("keyup", function () {
		$("#ePopupTxt2").html($("#ePopupTxt2Inp").val().replace(RegExp("\n","g"), "<br>"));
	});
	//Завершаем ввод данных таблички
	$("#ePopupBtn").on("click", function () {

		edgeTxt[0] = $("#ePopupTxt1Inp").val().replace(RegExp("\n","g"), "<br>");
		edgeTxt[1] = $("#ePopupTxt2Inp").val().replace(RegExp("\n","g"), "<br>");
		edgeTxt[2] = $("#ePopupFName option:selected").val();

		$(".edgeTxt2").html(edgeTxt[0]);
		$(".edgeTxt1").html(edgeTxt[1]);

		switch (edgeTxt[2]) {
			case "1":
				$(".edgeTxt1, .edgeTxt2").css("font-family", "Gothic");
				break;
			case "2" :
				$(".edgeTxt1, .edgeTxt2").css("font-family", "Goodvibes");
				break;
			case "3":
				$(".edgeTxt1, .edgeTxt2").css("font-family", "Ceremonious");
				break;
		}

		$("#engraving .step4lbhEdit").show();
		$("#shadowBox1").hide();
		$("#edgePopup").hide("puff", 500);
	});
	//Закрываем окно персонализации таблички без изменений
	$("#ePopupClose").on("click", function () {
		$("#engraving .step4lbhEdit").show();
		$("#shadowBox1").fadeOut(500);
		$("#edgePopup").hide("puff", 500);
	});

	imgZoom("box", 0, "boxStandard");
	imgZoom("box", 1, "boxRussia");
	imgZoom("box", 2, "boxMotore");
	imgZoom("cover", 0, "caseFirenze");
	imgZoom("cover", 1, "caseCarbon");
	imgZoom("cover", 2, "caseBianco");
	imgZoom("cover", 3, "caseMagenta");


	$("#clientPhone").mask("+7 (999) 999-99-99");
	$("clientEmail").mask("{1,20}@{1,20}.{2,6}[.{2}]");
// Переходим к оформлению
	$("#step4btn").on("click", function () {

		// получаем контейнер с html кодом нашей картинки
		var phoneBox = $("#phoneBox"); // phoneBox
		var phonePreview = $("#phonePreview");

		// копируем первоначальную модель в конечный div#phoneBox
		// присваиваем ему новый класс и удаляем старые
		$(".phoneBoxSelectedModel").remove();
		$(".step2model.mSelected").hide().clone().prependTo(phoneBox);
		$(phoneBox).find(".step2model.mSelected").removeClass('mSelected').removeClass('step2model').addClass('phoneBoxSelectedModel').css({
			'top' : '0px',
			'z-index' : '-1'
		}).show();

		curStep = 5;
		$("#step4left").hide("drop", {direction: "left"}, 800);
		$("#step4right").animate({left: "791px"}, 800);
		$(phoneBox).find(".phoneBoxSelectedModel").animate({left: "0px"}, 800);
		$("#step4main").animate({left: "161px"}, 800);
		$("#selOtionsConainer, #selOptTxt, #actControls4, #step4btn").fadeOut(800);
		$("#configStep5, #configStep5 #actControls5, #step5btn").fadeIn(800);

		var transformOriginOldValue = $(configPopup).css('transform');
		$(configPopup).css('transform', 'initial');

		// скрываем надписи(гравировки), потому что на данный момент их невозможно преобразовать в канвас
		$(".edgeTxt1, .edgeTxt2").toggleClass('dNone');

		html2canvas(phoneBox, {
			onrendered: function (canvas) {
				$("#phonePreviewCanvas").remove();
				$(phonePreview).append(canvas);
				$(phonePreview).find("canvas").attr('id', 'phonePreviewCanvas');
				getCanvas = canvas;
			}
		});

		// снова показываем надписи(гравировки)
		$(".edgeTxt1, .edgeTxt2").toggleClass('dNone');

		$(configPopup).css('transform', transformOriginOldValue);
	});

//При нажатии на иконку "Изменить" возвращаемся на шаг 4
	$("#acbModify").on("click", function () {
		curStep = 4;
		$(".phoneBoxSelectedModel").remove();
		$("#phonePreviewCanvas").remove();
		$(".step2model.mSelected").show();
		$("#step4left").show("drop", {direction: "left"}, 800);
		$("#step4right").animate({left: "1010px"}, 800);
		$(".mSelected").animate({left: "487px"}, 800);
		$("#step4main").animate({left: "380px"}, 800);
		$("#selOtionsConainer, #selOptTxt, #actControls4, #step4btn").fadeIn(800);
		$("#configStep5, #configStep5 #actControls5, #step5btn").fadeOut(800);
	});
//При нажатии кнопки "Поделиться"
	$("#acbShare").on("click", function () {
		$(".pluso").toggle("clip", 300);

	});



//Убираем подсветку при фокусе на полях ввода
	$("#clientName, #clientPhone, #clientEmail").on("focus", function () {
		$(this).removeClass("inpError");
	});

//Отправка заказа
	$("#step5btn").on("click", function () {
		var inpError = 0;
		var emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
		if ($("#clientName").val() == "") {
			$("#clientName").addClass("inpError");
			inpError++;
		}
		if ($("#clientPhone").val() == "") {
			$("#clientPhone").addClass("inpError");
			inpError++;
		}
		if ( $("#clientEmail").val() == ""  || !emailPattern.test($("#clientEmail").val()) ) {
			$("#clientEmail").addClass("inpError");
			inpError++;
		}
		if (inpError == 0) {
			//форма прошла валидацию
			yaCounter38456575.reachGoal('zakaz_konstruktor');
			$("#cEmail").text($("#clientEmail").val());
			//Формируем текст заказа
			var msgA = "", msgC = "", msgTemp = "";
			msgTemp += "<p><b>Имя клиента:</b> "+$("#clientName").val()+"</p>";
			msgTemp += "<p><b>Телефон клиента:</b> "+$("#clientPhone").val()+"</p>";
			msgTemp += "<p><b>Email клиента:</b> "+$("#clientEmail").val()+"</p>";
			if ($("#clientSay").val() != "") {
				msgTemp += "<p><b>Сообщение:</b> "+$("#clientSay").val()+"</p>";
			}
			msgTemp += "<hr>";
			msgA += "<p><b>Модель:</b> "+curModelName+"</p>";
			msgA += "<p><b>" + master[0][1] + ":</b> " + master[1][1] + "</p>";
			for (i=0; i<mainParams.length; i++) {
				for (j=1; j<mainParams[i].length; j++) {
					if (mainParams[i][j][0] == 1) {
						msgA += "<p><b>"+mainParams[i][0][1]+":</b> "+mainParams[i][j][3]+"</p>";
						if (mainParams[i][0][0] == "table") {
							msgA += "<p>Текст таблички:<br>"+tableTxt[0]+"</p>";
							msgA += "<p>Шрифт №"+tableTxt[1]+"</p>";
							msgA += "<p>Размер шрифта "+tableTxt[2]+" пт</p>";
						}
						if (mainParams[i][0][0] == "engraving") {
							msgA += "<p>Текст гравироки 1: "+edgeTxt[0]+"</p>";
							msgA += "<p>Текст гравироки 2: "+edgeTxt[1]+"</p>";
							msgA += "<p>Шрифт №"+edgeTxt[2]+"</p>";
						}
					}
				}
			}

			// вставляем изображение
			var sendImageData = getCanvas.toDataURL("image/png");
			//console.log(sendImageData);
			//msgA += "<p><img alt='' src='" + sendImageData + "'></p>";

			msgC = msgA;

			msgA = msgTemp + msgA + "<p><b>Примерная стоимость:</b> "+$("#totalCostVal").text()+"</p>";

			//Отправляем сообщение
			jQuery.ajax({
				type: "POST",
				url: "config/php/send.php",
				data: {
					adminMessage: msgA,
					clientMessage: msgC,
					clientEmail: $("#clientEmail").val(),
					sendImageData : sendImageData
				},
				success: function() {
					var fpWidth = $("#finishPopup").width();
					var fpHeight = $("#finishPopup").height();
					var shb1Width = $("#shadowBox1").width();
					var shb1Height = $("#shadowBox1").height();
					$("#finishPopup").css("left",(shb1Width - fpWidth)/2);
					$("#finishPopup").css("top",(shb1Height - fpHeight)/2);
					$("#shadowBox1").fadeIn(500);
					$("#finishPopup").show("scale", 500);
				},
				error: function() {
					alert("При отправке заказа произошла ошибка!");
				}
			});
			$("#clientName, #clientPhone, #clientEmail, #clientSay").val("");
		}
	});
//Закрываем сообщение об успешной отправке
	$("#fPopupBtn").on("click", function () {
		$("#shadowBox1, #finishPopup").hide("puff", 500);
	});
});

//Показываем коробку/чехол в полный размер при наведении на иконку
function imgZoom(blockId, i, imgId) {
	$("#"+blockId+" .step4lbbElement img").eq(i).on("mouseenter", function () {
		$("#"+imgId).fadeIn(500);
	});
	$("#"+blockId+" .step4lbbElement img").eq(i).on("mousemove", function (e) {
		$("#"+imgId).css("left", e.pageX+"px");
		$("#"+imgId).css("top", (e.pageY - $("#"+imgId).height() - 50) + "px");
	});
	$("#"+blockId+" .step4lbbElement img").eq(i).on("mouseleave", function () {
		$("#"+imgId).fadeOut(500);
	});
}
//Раскрываем спойлер
function spoiler () {

}


function spoilerShow (block) {
	if ($("#" + block + " .step4lbBody").css("display") == "none") {
		$(".step4lbBody:not(.blockVisible)").hide("blind", 300);
		$("#" + block + " .step4lbhDown").addClass("rotate180");
		$("#" + block + " .step4lbBody").show("blind", 300);
		$("#step4left").animate({opacity: 1}, 400).niceScroll({
			cursorcolor:"#8b8b8b",
			cursorwidth: "4px",
			cursorborder: "0",
			cursorborderradius: "2px",
			zindex: "1005",
			hidecursordelay: "1200",
			autohidemode: true,
			boxzoom:false
		});
	}
}

//Устанавливаем флаг на выбранной опции
function setFlag (option, index) {
	for (i=1; i<option.length; i++){ //Сбрасываем флаги всех опций
		option[i][0] = 0;
	}
	option[index][0] = 1; //Ставим флаг на выбранной опции
}

//Сбрасываем флаги всех опций
function clearFlag (optionArr) {
	for (i=0; i<optionArr.length; i++) {
		for (j=1; j<optionArr[i].length; j++){
			optionArr[i][j][0] = 0;
		}
	}
}

//Возвращаем ячейку параметра с флагом 1
function curOption (option) {
	for (i=1; i<option.length; i++) {
		if (option[i][0] == 1) {
			return option[i];
		}
	}
}
function curOptionIndex (option) {
	for (i=1; i<option.length; i++) {
		if (option[i][0] == 1) {
			return i;
		}
	}
}
//Рассчитываем стоимость
function calcBasePrice () {
	var cost = 0;
	for (i=0; i<costBaseArr.length; i++) {
		for (j=1; costBaseArr[i].length; j++) {
			if (costBaseArr[i][j][0] == 1) {
				cost += costBaseArr[i][j][2];
				break;
			}
		}
	}
	basePrice = cost;
	return cost;
}

//Разделение разрядов числа, то есть из 1000000 сделать 1 000 000
function dSpace (val) {
	return val.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1\u00A0');
}

// !!!
function render () {
	//Проявляем или скрываем изображения в зависимости от флага
	var imgIndex;
	if (model[1][0] == 1) {
		imgIndex = 7;
	} else if (model[2][0] == 1) {
		imgIndex = 8;
	} else if (model[3][0] == 1){
		imgIndex = 9;
	}
	for (i=0; i<mainParams.length; i++) {
		var x = 0;
		for (j=1; j<mainParams[i].length; j++) {
			x += mainParams[i][j][0];
			//Если параметр активирован
			if (mainParams[i][j][0] == 1) {
				//Выделяем соответствующую иконку в левой панели
				$("#"+mainParams[i][0][0]+" .step4lbbElement").eq(j-1).removeClass("paramNotSelected");
				//Показываем картинку в основном окне
				var iks = 1;
				if (mainParams[i][0][0] == "plateMaterial") {
					for (m=1; m<plateGoldPattern.length; m++) {
						if (plateGoldPattern[m][0] == 1){
							iks = 0;
						}
					}
					for (m=1; m<plateTitanPattern.length; m++) {
						if (plateTitanPattern[m][0] == 1){
							iks = 0;
						}
					}
				}
				if (mainParams[i][0][0] == "frameMaterial") {
					for (m=1; m<frameGoldPattern.length; m++) {
						if (frameGoldPattern[m][0] == 1){
							iks = 0;
						}
					}
					for (m=1; m<frameTitanPattern.length; m++) {
						if (frameTitanPattern[m][0] == 1){
							iks = 0;
						}
					}
				}

				if ($(mainParams[i][j][imgIndex])) {
					if (iks == 1) {
						$(mainParams[i][j][imgIndex]).fadeIn(500);
					} else {
						$(mainParams[i][j][imgIndex]).fadeOut(500);
					}
				} else {

				}
			} else { //Если параметр дизактивирован
				$("#"+mainParams[i][0][0]+" .step4lbbElement").eq(j-1).addClass("paramNotSelected");
				//Скрываем картинку в основном окне
				if ($(mainParams[i][j][imgIndex])) {
					$(mainParams[i][j][imgIndex]).fadeOut(500);
				}
			}
		}
		if (x == 0) {	//Не выбран ни один элемент
			$.each($("#"+mainParams[i][0][0]+" .step4lbbElement"), function () {
				$(this).removeClass("paramNotSelected");
			});
		}
	}
	//Считаем стоимость, записываем в спецификацию
	if (box[2][0] == 0 && box[3][0] == 0) { //Если не выбраны коробки, берем стандартную
		box[1][0] = 1;
	}
	var cost = basePrice + master[1][2];
	var spec = specLine(1, "" + curModelName, basePrice, 0, 0);
	spec += specLine(2, master[0][1] + " (" + master[1][1] + ")", master[1][2], 0, 0);
	var num = 2;
	for (i=0; i<costElementsArr.length; i++) {
		for (j=1; j<costElementsArr[i].length; j++) {
			//Если параметр активирован
			if (costElementsArr[i][j][0] == 1) {
				num++;
				//Прибавка к стоимости
				if (typeof(cost) == "number") {
					if (typeof(costElementsArr[i][j][5]) == "number") {
						cost += costElementsArr[i][j][5]; //Прибавляем стоимость
					}
				}
				//Запись в спецификацию
				spec += specLine(num, costElementsArr[i][j][4], costElementsArr[i][j][5], i, j);
			}
		}
	}
	cost = dSpace(cost) + ",-";

	$("#totalCostVal").html(cost);
	$("#selOptions").html(spec);
	$(".optBlock").eq(0).append("<div id='backToStep3'>Выбрать другую модель</div>"); //Добавляем пункт возврата к выбору модели
	$(".optDel").eq(0).css("display", "none"); //Скрываем символ "Удалить" для первой строки
	$(".optDel").eq(1).css("display", "none"); //Скрываем символ "Удалить" для второй строки
	$(".optDel").eq(2).css("display", "none"); //Скрываем символ "Удалить" для третьей строки
	if ($(".optDel").eq(3)) {
		$(".optDel").eq(3).hide(); //Скрываем символ "Удалить" для третьей строки
	}
}

//
function undo () {
	if ($(this).hasClass("acbDisable")) {
	} else {
		$("#acbUndo").addClass("acbDisable");
		$("#acbRedo").removeClass("acbDisable");
		for (i=0; i<mainParams.length; i++) {
			for (j=1; j<mainParams[i].length; j++) {
				mainParams[i][j][2] = mainParams[i][j][0];
				mainParams[i][j][0] = mainParams[i][j][1];
				mainParams[i][j][1] = mainParams[i][j][2];
			}
		}
		render();
		//Записываем видимость опций до отката назад
		blockVisRedo = [];
		bodyVisRedo = [];
		$.each($(".step4block"), function () {
			blockVisRedo.push($(this).css("display"));
		});
		$.each($(".step4lbBody"), function () {
			bodyVisRedo.push($(this).css("display"));
		});
		//Делаем откат назад видимости опций
		$.each(bodyVisUndo, function (i, val) {
			if (val == "none") {
				$(".step4lbBody").eq(i).fadeOut(500);
			} else {
				$(".step4lbBody").eq(i).fadeIn(500);
			}
		});
		$.each(blockVisUndo, function (i, val) {
			if (val == "none") {
				$(".step4block").eq(i).fadeOut(500);
			} else {
				$(".step4block").eq(i).fadeIn(500);
			}
		});
	}
}


//
function redo () {
	if ($(this).hasClass("acbDisable")) {
	} else {
		$("#acbRedo").addClass("acbDisable");
		$("#acbUndo").removeClass("acbDisable");
		for (i=0; i<mainParams.length; i++) {
			for (j=1; j<mainParams[i].length; j++) {
				mainParams[i][j][2] = mainParams[i][j][0];
				mainParams[i][j][0] = mainParams[i][j][1];
				mainParams[i][j][1] = mainParams[i][j][2];
			}
		}
		render();
		//Записываем видимость опций до отката вперед
		blockVisUndo = [];
		bodyVisUndo = [];
		$.each($(".step4block"), function () {
			blockVisUndo.push($(this).css("display"));
		});
		$.each($(".step4lbBody"), function () {
			bodyVisUndo.push($(this).css("display"));
		});
		//Делаем откат вперед видимости опций
		$.each(bodyVisRedo, function (i, val) {
			if (val == "none") {
				$(".step4lbBody").eq(i).fadeOut(500);
			} else {
				$(".step4lbBody").eq(i).fadeIn(500);
			}
		});
		$.each(blockVisRedo, function (i, val) {
			if (val == "none") {
				$(".step4block").eq(i).fadeOut(500);
			} else {
				$(".step4block").eq(i).fadeIn(500);
			}
		});
	}
}

//
function undoWrite () {
	for (i=0; i<mainParams.length; i++) {
		for (j=1; j<mainParams[i].length; j++) {
			mainParams[i][j][1] = mainParams[i][j][0];
		}
	}
	$("#acbRedo").addClass("acbDisable");
	$("#acbUndo").removeClass("acbDisable");
}

//Добавляем строку в перечень опций
function specLine (num, name, cost, i, j) {
	if (typeof(cost) == "number") {
		cost = dSpace(cost) + ",-";
	}
	var specLineVal = "<div class='optContainer'><div class='optNum'>"+num+".</div><div class='optBlock'><div class='optName'>"+name+"</div><div class='optDel' data-param="+i+"%"+j+"></div><div class='optCost'>"+cost+"</div></div><div class='cBoth'></div></div>";
	return specLineVal;
}

//Cмена картинки смартфонов
function changeSrc (srcArr) { //[srcSe, src6S, src6sp]
	var imgArr = ["#iphone6S img", "#iphone7 img", "#iphone7Plus img"]
	for (i=1; i<model.length; i++) {
		$(imgArr[i-1]).attr("src", srcArr[i-1]);
	}
}