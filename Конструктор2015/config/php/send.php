<?
require 'class.phpmailer.php';

if (isset($_POST['adminMessage'])){

        $mailA = new PHPMailer;
        $mailC = new PHPMailer;

        $mailA->CharSet='UTF-8';
        $mailC->CharSet='UTF-8';

        $image_data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['sendImageData']));
        $image_name = date('Y-m-j-h-i-s') . '.png';
        $image_path = $_SERVER['DOCUMENT_ROOT'] . '/images/orders/' . $image_name;
        file_put_contents($image_path, $image_data);

        $mailA->addAddress('caviar@perlapenna.com');
        $mailA->addAddress('anton@perlapenna.com');
        $mailC->addAddress($_POST['clientEmail']);
        $mailA->setFrom('caviar@perlapenna.com', 'Caviar Atelier');
        $mailC->setFrom('caviar@perlapenna.com', 'Caviar Atelier');

        $mailA->Subject = 'Заказ iPhone';
        $mailC->Subject = 'Ваш индивидуальный заказ в Ателье Caviar принят';

        $mailA->addAttachment($image_path, $image_name);
        $mailC->addAttachment($image_path, $image_name);
        $mailA->isHTML(true);
        $mailC->isHTML(true);

        $mailA->Body = $_POST['adminMessage'];
        $mailC->Body = $_POST['clientMessage'];
        $mailA->AltBody = $_POST['adminMessage'];
        $mailC->AltBody = $_POST['clientMessage'];

        $mailA->send();
        $mailC->send();

}
?>