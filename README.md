Описание
=========

Много кода под запретом о распространении. Покажу при встрече.

1. Шаблон стратегия - папка, в которой реализован шаблон проектирования на PHP "Стратегия". Он позволяет легко перелючаться между использованием функций локаной работы с файлами и работы в Azure

2. Фоновый процесс.
Это фоновый воркер, который работает с данными из API Instagram.
Он умеет отправлять результаты работы обратно на клиент через веб-сокеты в любой момент времени. И это классно :)

3. Models.
Это пример модели из MVC. В файле есть примеры функций с логикой построения запросов

4. Виджет
sarafan.min.js - это обфусцированный Javascript, но по нему видно, какие технологии используются. Основной рабочий код - в конце файла
Код взаимодействует с удаленным API, в зависимости от настроек по довольно сложной логике рендерит новый HTML виджета на клиенте.
Используются такие штуки, как MutationObserver, оптимизация производительности, прототипы, самописные слайдеры, промисы, самописный SDK для ваимодействия с апи и др.

5. Конструктор 2015.
Это конструктор элитных телефонов на JS + JQuery. После интерактивного создания пользователем - отправляется картика на почу к администратору сайта.

6. Vue Protuhator.
Страница для замены старых товаров из фидов на новые.
Пример страницы на Vue, вшитой в YII 2.