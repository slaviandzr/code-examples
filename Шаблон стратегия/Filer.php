<?php
/**
 * Created by PhpStorm.
 * User: lukoyanov
 * Date: 19.04.17
 * Time: 13:17
 */

namespace common\components;

/**
 * Microsoft Azure my implementation
 *
 *
 * @param FilerStrategy $filerStrategy
 */
class Filer
{
    private $filerStrategy;

    function __construct(FilerStrategy $strategy)
    {
        $this->filerStrategy = $strategy;
    }

    function execute()
    {
        $result = $this->filerStrategy->test();

        return $result;
    }

    function saveFileFromUrl($containername, $filename, $url){
        $result = $this->filerStrategy->saveFileFromUrl($containername, $filename, $url);

        return $result;
    }

    public function getProductsFromSarafanAPIForImage($containername, $filename, $url){
        $result = $this->filerStrategy->getProductsFromSarafanAPIForImage($containername, $filename, $url);

        return $result;

    }

    public function constructFileUrl($container, $blobname){
        $result = $this->filerStrategy->constructFileUrl($container, $blobname);

        return $result;
    }



}