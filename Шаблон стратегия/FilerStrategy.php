<?php
/**
 * Created by PhpStorm.
 * User: lukoyanov
 * Date: 19.04.17
 * Time: 13:07
 */

namespace common\components;


interface FilerStrategy
{
    function init ();
    function saveFile ($containername, $filename, $filepath);
    function saveFileFromUrl ($containername, $filename, $url);
    function getProductsFromSarafanAPIForImage ($containername, $filename, $url);
    function deleteFile($container, $blobname, $filepath);
    function listFiles ($containername);
    function getFile ($containername, $filename);
    function constructFileUrl($container, $blobname);
}