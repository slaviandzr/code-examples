<?php

namespace common\components;

use Yii;
use yii\base\Exception;


/**
 * Local file storage implementation my implementation
 *
 *
 * @author Vyacheslav Lukoyanov
 * @since 2017
 * @copyright
 * @version 1.0
 * @license BSD http://www.opensource.org/licenses/bsd-license.php
 */
class FilerLocalStrategy implements FilerStrategy {

    public $documentRoot;
    public $initFolder;
    public $folderMax;
    public $folderStandart;

    public function __construct(){
        $this->documentRoot = Yii::$app->params['documentRoot'];
        $this->initFolder = $this->documentRoot . '/' . Yii::$app->params['localImagesDir'] . '/';
        $this->folderMax = $this->initFolder . 'img-max/';
        $this->folderStandart = $this->initFolder . 'img-standart/';

        $this->checkPermissions($this->initFolder);
        $this->checkPermissions($this->folderMax);
        $this->checkPermissions($this->folderStandart);
    }

    public function checkPermissions($path){
        if ( file_exists($path) && is_dir($path)){
            if ( !is_writable($path) ){
                chmod($path, 0777);
            }
        } else {
            mkdir($path, 0777, true);
        }
    }

    public function init(){
        // TODO create necessary folders and set rights
    }

    public function saveFile($containername, $filename, $filepath)
    {
        // TODO: Implement saveFile() method.
    }

    public function saveFileFromUrl($containername, $filename, $url)
    {
        // save to local storage from web protocols
        $ch = curl_init($url);
        $image_src = $this->initFolder . $containername . '/' . $filename;
        $fp = fopen($image_src, 'w+');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        if (!curl_exec($ch)){
            throw new Exception('Function saveFileFromUrl: curl_exec not work.');
        }
        curl_close($ch);
        fclose($fp);
    }

    public function deleteFile($container, $blobname, $filepath = null)
    {

        if(!$filepath){
            $filepath = $this->constructFilePath($container, $blobname);
        }

        if(file_exists($filepath)){
            return unlink($filepath);
        } else {
            return false;
        }
    }

    public function listFiles($containername)
    {
        // TODO: Implement listFiles() method.
    }

    public function getFile($containername, $filename)
    {
        // TODO: Implement getFile() method.
    }

    public function constructFileUrl($containername, $filename)
    {
        $url = Yii::$app->params['defaultEndpointsProtocol'] . '://' .
            Yii::$app->params['host'] . '/' .
            Yii::$app->params['localImagesDir'] . '/' .
            $containername . '/' . $filename;

        return $url;
    }

    public function constructFilePath($containername, $filename)
    {
        $path = Yii::getAlias('@webroot') . '/' .
            Yii::$app->params['localImagesDir'] . '/' .
            $containername . '/' . $filename;

        return $path;
    }

    public function getProductsFromSarafanAPIForImage($containername, $filename, $url)
    {
        if (!empty($url)){
            // for local strategy we don't work with remote pictures
            unset($url);
        }

        $filepath = $this->constructFilePath($containername, $filename);
        $link = Yii::$app->params['sarafan_api_test_url'];
        $cfile = curl_file_create($filepath,'image/jpeg','test_name.jpg');

        $curl=curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $link,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array("image" => $cfile, "category" => 35, "gender" => 2),
            CURLOPT_HTTPHEADER => array('X-Requested-With: XMLHttpRequest')
        ));

        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);

        return $result;
    }




}
