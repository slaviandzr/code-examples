<?php

namespace common\components;

use Yii;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Blob\Models\Blob;
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
// To process exceptions you need
use MicrosoftAzure\Storage\Common\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use yii\base\Exception;



/**
 * Microsoft Azure my implementation
 *
 *
 * @author Vyacheslav Lukoyanov
 * @since 2017
 * @copyright
 * @version 1.0
 * @license BSD http://www.opensource.org/licenses/bsd-license.php
 *
 * @param BlobRestProxy $blobRestProxy
 */
class FilerAzureStrategy implements FilerStrategy {

    public $blobRestProxy;
    public $defaultEndpointsProtocol;
    public $accountName;
    public $endpointSuffix;

    private $connectionString;

    public function __construct(){

        // Create blob REST proxy.
        $this->connectionString = 'DefaultEndpointsProtocol=' . Yii::$app->params['azure']['DefaultEndpointsProtocol'] . ';' .
            'AccountName=' . Yii::$app->params['azure']['AccountName'] . ';' .
            'AccountKey=' . Yii::$app->params['azure']['AccountKey'];
        $this->blobRestProxy = ServicesBuilder::getInstance()->createBlobService($this->connectionString);
        $this->defaultEndpointsProtocol = Yii::$app->params['azure']['DefaultEndpointsProtocol'];
        $this->accountName = Yii::$app->params['azure']['AccountName'];
        $this->endpointSuffix = Yii::$app->params['azure']['EndpointSuffix'];

    }

    public function init(){
        // OPTIONAL: Set public access policy and metadata.
        // Create container options object.
        $createContainerOptions = new CreateContainerOptions();

        // Set public access policy. Possible values are
        // PublicAccessType::CONTAINER_AND_BLOBS and PublicAccessType::BLOBS_ONLY.
        // CONTAINER_AND_BLOBS:
        // Specifies full public read access for container and blob data.
        // proxys can enumerate blobs within the container via anonymous
        // request, but cannot enumerate containers within the storage account.
        //
        // BLOBS_ONLY:
        // Specifies public read access for blobs. Blob data within this
        // container can be read via anonymous request, but container data is not
        // available. proxys cannot enumerate blobs within the container via
        // anonymous request.
        // If this value is not specified in the request, container data is
        // private to the account owner.
        $createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);

        try    {
            // Create container.
            $this->blobRestProxy->createContainer("img-standart", $createContainerOptions);
            $this->blobRestProxy->createContainer("img-max", $createContainerOptions);
            $this->blobRestProxy->createContainer("img-avatars", $createContainerOptions);
        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";
        }
    }

    public function saveFile($container, $blobname, $filepath)
    {
        $content = fopen($filepath, "r");

        try    {
            //Upload blob
            $this->blobRestProxy->createBlockBlob($container, $blobname, $content);
            return true;
        }
        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code . ": " . $error_message . "<br />";
            return false;
        }
    }

    public function saveFileFromUrl($container, $blobname, $url)
    {
        $content = fopen($url, "r");

        try    {
            //Upload blob
            $this->blobRestProxy->createBlockBlob($container, $blobname, $content);
            return true;
        }
        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code . ": " . $error_message . "<br />";
            return false;
        }
    }

    /**
     * get products
     * @var $container string container from Azure
     * @var $blobname string blob from Azure
     * @var $url string link of image
     *
     * @return array
     */
    public function getProductsFromSarafanAPIForImage($container, $blobname, $url)
    {
        try {
            $url = $this->constructFileUrl($container, $blobname);
            $raw = MiscHelpers::getRawFromLink($url);
            $link = MiscHelpers::getCurrentApiLink();

            // formation of object witch containing file
            $file = new OFile('test_name.jpg', 'image/jpeg', $raw);

            $delimiter = '-------------' . uniqid();
            $post = MiscHelpers::createPostForSendingToApi($delimiter, $file);
            $result = MiscHelpers::getResultsFromApiPost($link, $post, $delimiter);

            if (!is_array($result)){
                throw new Exception('Not the array has come');
            }

            if (empty($result)){
                throw new Exception('The empty array with products has come');
            }



        } catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";
        }

        return $result;

    }

    public function deleteFile($container, $blobname, $filepath = null)
    {
        try {
            // Delete blob.
            $this->blobRestProxy->deleteBlob($container, $blobname);
        }
        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";
        }
    }

    /**
     * List Blobs
     *
     *
     * @return object
     */

    public function listFiles($container)
    {
        try {
            // List blobs.
            $blob_list = $this->blobRestProxy->listBlobs($container);
            $blobs = $blob_list->getBlobs();


        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";

            return null;
        }

        return $blobs;
    }

    /**
     * get Blob
     *
     *
     * @return Blob
     */
    public function getFile($container, $blobname)
    {
        try {
            $listBlobsOptions = new ListBlobsOptions();
            $listBlobsOptions->setPrefix($blobname);
            $blob_list = $this->blobRestProxy->listBlobs($container, $listBlobsOptions);
            $blobs = $blob_list->getBlobs();
            return array_pop($blobs);
        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";
        }
    }

    /**
     * get Blob Url from api
     *
     * @return string
     */
    public function getFileUrlFromAzure($container, $blobname)
    {
        return $this->getFile($container, $blobname)->getUrl();
    }

    /**
     * fast construct blob Url
     *
     *
     * @return string
     */
    public function constructFileUrl($container, $blobname)
    {
        $url = $this->defaultEndpointsProtocol . '://' .
            $this->accountName . '.blob.' .
            $this->endpointSuffix . '/' .
            $container . '/' . $blobname;

        return $url;
    }


}
