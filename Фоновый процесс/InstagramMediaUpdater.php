<?php

namespace common\jobs;

use common\models\Objects;
use common\models\SmMediaProducts;
use Yii;
use shakura\yii2\gearman\JobBase;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use console\components\ChatServer;
use common\models\UsersSmAccounts;
use common\models\UsersSmMedia;
use common\components\Instagram;
use common\components\Filer;
use common\components\FilerAzureStrategy;
use common\components\FilerLocalStrategy;

use React\ZMQ\Context;

use React;

/**
 * Update or add new posts to DB from Instagram in background mode *
 *
 * @property Filer $_filer
 */

class InstagramMediaUpdater extends JobBase
{
    public static $noText = 'No instagram argument';

    private $_filer;

    /**
     *
     * @param $instagram Instagram
     */
    public function execute(\GearmanJob $job = null)
    {
        echo 'start media update' . PHP_EOL;
        $params = $this->getWorkload($job)->getParams();
        $instagram = $params['instagram'];
        $access_token = $params['access_token'];
        $account_id = $params['account_id'];
        $user = $params['user'];

        if ( strtolower(Yii::$app->params['filerStrategy'])  == 'azure'){
            $this->_filer = new Filer(new FilerAzureStrategy());
        } else {
            $this->_filer = new Filer(new FilerLocalStrategy());
        };

        $context = new \ZMQContext();
        $push = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my_media_updater');
        $push->connect("tcp://127.0.0.1:5555");

        $pushMessage = [
            'category' => 'sarafan.tech/cabinet/update',
            'currentPercent' => 0,
            'status' => 'starting',
            'message' => 'Starting...'
        ];
        $push->send(json_encode($pushMessage));

        if (!empty($params['instagram'])){

            $instagram->setAccessToken($access_token);
            $limit = ( empty( $params['limit'] ) ) ? Yii::$app->params['instagramGetUserMediaLimit'] : $params['limit'];
            $userMedia = $instagram->getUserMedia('self', $limit);
            if (!empty($userMedia)) {
                $account = UsersSmAccounts::findOne($account_id);

                if( !empty( $params['accountStatusToActive'] ) && $params['accountStatusToActive'] ){
                    $accountStatusToActive = true;
                } else {
                    $accountStatusToActive = false;
                }

                if (!$account->checkPermissionOfUpdates()) return false;
                $mediaCount = count($userMedia->data);
                $iterator = 0;

                echo PHP_EOL . count($userMedia->data) . PHP_EOL;
                $reversed_data = array_reverse($userMedia->data);

                foreach ( $reversed_data as $media ){
                    UsersSmMedia::saveFromApi($media, $account, $this->_filer, $media->id, $media->user->id, true, $accountStatusToActive);

                    $iterator++;

                    $pushMessage = [
                        'category' => 'sarafan.tech/cabinet/update',
                        'currentPercent' => (int)($iterator * 100 / $mediaCount),
                        'status' => 'loading',
                        'message' => 'Loading...'
                    ];
                    $push->send(json_encode($pushMessage));
                }

                Yii::$app->session->setFlash('success', 'Object was successfully loaded');
            }

            $pushMessage = [
                'category' => 'sarafan.tech/cabinet/messages',
                'message' => 'Your Instagram medias successfully loaded',
                'from' => 'Sarafan'
            ];
            $push->send(json_encode($pushMessage));

            $pushMessage = [
                'category' => 'sarafan.tech/cabinet/update',
                'currentPercent' => 100,
                'status' => 'success',
                'message' => 'Loading success'
            ];
            $push->send(json_encode($pushMessage));

        } else {

            $pushMessage = [
                'category' => 'sarafan.tech/cabinet/update',
                'currentPercent' => 0,
                'status' => 'error',
                'message' => 'Loading error'
            ];
            $push->send(json_encode($pushMessage));

            Yii::info(self::$noText);
        }


        return true;

    }
}